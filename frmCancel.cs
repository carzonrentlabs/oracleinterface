using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.OracleClient;

namespace OracleInterface
{
    public partial class frmCancel : Form
    {
        public frmCancel()
        {
            InitializeComponent();
        }
        int intBeginID = 0;
        int intEndID = 0;
        private void frmCancel_Load(object sender, EventArgs e)
        {
            
            string strConnection = System.Configuration.ConfigurationManager.AppSettings["corConnectString"];
            string strQuery;
            SqlConnection oConnection = new SqlConnection(strConnection);
            strQuery = " SELECT CityID, CityName FROM CORIntCityMaster WHERE Active = 1";
            SqlCommand objCommand = new SqlCommand(strQuery.ToString(), oConnection);
            SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
            DataSet objDataset = new DataSet();
            objAdapter.Fill(objDataset);
            cboLocation.DataSource = objDataset.Tables[0];
            cboLocation.DisplayMember = "CityName";
            cboLocation.ValueMember = "CityID";
            //cboLocation.Items.Add(objDataset.Tables[0].Columns[0]);
            objCommand.Dispose();
            objAdapter.Dispose();
            objDataset.Dispose();
            oConnection.Close();
            oConnection.Dispose();
            //fn_FillID();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            // Connection from Insta-Server
            string strConnection = System.Configuration.ConfigurationManager.AppSettings["corConnectString"];
            StringBuilder strQuery;
            SqlConnection oConnection = new SqlConnection(strConnection);

            string strLocation = cboLocation.SelectedValue.ToString();
            string strFromDate = fromDate.Text;
            string strToDate = toDate.Text;

            //strQuery = new StringBuilder(" SELECT * FROM  fn_DataForVoidOracleSale('" + strFromDate + "', '" + strToDate + "', " + strLocation + ") ");
            strQuery = new StringBuilder("  SELECT OldInv FROM  vVoidBookingNewold");
                //+ strFromDate + "', '" + strToDate + "', " + strLocation + ") ");

            SqlCommand objCommand = new SqlCommand(strQuery.ToString(), oConnection);
            SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
            DataSet objDataset = new DataSet();
            objAdapter.Fill(objDataset);

            // New connection string for local connection
            string strLocalConnection = System.Configuration.ConfigurationManager.AppSettings["localConnectString"];
            OleDbConnection oLocalConnection = new OleDbConnection(strLocalConnection);
            oLocalConnection.Open();
            OleDbCommand localCommand;
            OleDbCommand localCommand1;
            OleDbDataAdapter localAdapter;
            DataSet localDS;

            StringBuilder strSearch;
            StringBuilder strUpdate;
            // Check for Invoice Number exist in Invoice_Dec table
            for (int i = 0; i < objDataset.Tables[0].Rows.Count; i++)
            {
                strSearch = new StringBuilder(" SELECT * FROM Invoice_Dec WHERE Invoice_No = 'InvCOR" + objDataset.Tables[0].Rows[i][0].ToString() +"'");
                //strSearch = new StringBuilder(" SELECT * FROM Invoice_Dec WHERE right(invoice_no,6) = '" + objDataset.Tables[0].Rows[i][0].ToString() + "'");
                localCommand = new OleDbCommand(strSearch.ToString(), oLocalConnection);
                localAdapter = new OleDbDataAdapter(localCommand);
                localDS = new DataSet();
                localAdapter.Fill(localDS);

                // If Invoice_No exist
                //if (localDS.Tables[0].Rows)
                    if (localDS.Tables[0].Rows.Count > 0)
                {
                    // Check for Upload Status
                    if (Convert.ToBoolean(localDS.Tables[0].Rows[0]["Uploaded"].ToString()) == true)
                    {
                        // Sale is uploaded
                        // Check for New_Car is VOID or not
                        if (localDS.Tables[0].Columns["New_Car"].ToString() != "VOID")
                        {
                            strUpdate = new StringBuilder(" UPDATE Invoice_Dec SET Uploaded = 1, New_Car = 'VOID' WHERE Invoice_No = 'InvCOR" + objDataset.Tables[0].Rows[i][0].ToString() + "'");
                            //strUpdate = new StringBuilder(" UPDATE Invoice_Dec SET Uploaded = 0, New_Car = 'VOID' WHERE right(invoice_no,6) = '" + objDataset.Tables[0].Rows[i][0].ToString() + "'");
                            localCommand1 = new OleDbCommand(strUpdate.ToString(), oLocalConnection);
                            localCommand1.ExecuteNonQuery();
                        }
                        else
                        {
                            // Sale already reverse
                            // Delete record for Server dataset
                            objDataset.Tables[0].Rows[i].Delete();

                        }// End of New_Car checking
                    }
                    else
                    {
                        strUpdate = new StringBuilder(" UPDATE Invoice_Dec SET Uploaded = 1, New_Car = 'VOID' WHERE invoice_no = 'InvCOR" + objDataset.Tables[0].Rows[i][0].ToString() + "'");
                        localCommand1 = new OleDbCommand(strUpdate.ToString(), oLocalConnection);
                        localCommand1.ExecuteNonQuery();

                        // Delete record for Server dataset
                        objDataset.Tables[0].Rows[i].Delete();
                    }// End of Upload Status
                }
                else
                {
                    // Delete record for Server dataset
                    objDataset.Tables[0].Rows[i].Delete();
                }// End of Invoice checking
            }// End of for loop
            objDataset.AcceptChanges();

            string strDelete = "DELETE FROM InvImp ";
            localCommand1 = new OleDbCommand(strDelete, oLocalConnection);
            localCommand1.ExecuteNonQuery();

            StringBuilder strInsert;

            // Create connection with oracle DB
            string strOraConnection = System.Configuration.ConfigurationManager.AppSettings["oracleConnection"];
            OracleConnection oraConnection = new OracleConnection(strOraConnection);
        
            oraConnection.Open();
            StringBuilder strOraQuery;

            for (int j = 0; j < objDataset.Tables[0].Rows.Count; j++)
            {
                strOraQuery = new StringBuilder(" select distinct trx_number, customer_trx_line_id, b.description from  ");
                strOraQuery.Append(" ra_customer_trx_all a, ra_customer_trx_lines_all b ");
                strOraQuery.Append(" where a.customer_trx_id = b.customer_trx_id ");
                strOraQuery.Append(" and line_type = 'LINE' and trx_number = 'InvCOR" + objDataset.Tables[0].Rows[j][0].ToString() + "'");
                OracleCommand cmdOraQuery = new OracleCommand(strOraQuery.ToString(), oraConnection);
                OracleDataReader drOraQuery = cmdOraQuery.ExecuteReader();

                if (drOraQuery != null)
                {
                    while (drOraQuery.Read())
                    {
                        strInsert = new StringBuilder("INSERT INTO InvImp(trx_number, customer_trx_line_id, description) ");
                        strInsert.Append("VALUES('" + drOraQuery["trx_number"].ToString() + "', '" + drOraQuery["customer_trx_line_id"].ToString() + "', '" + drOraQuery["description"].ToString() + "' )");
                        localCommand1 = new OleDbCommand(strInsert.ToString(), oLocalConnection);
                        localCommand1.ExecuteNonQuery();
                    }

                }// End of drOraQuery
                
            }// End of for loop

            oLocalConnection.Close();
            oraConnection.Close();
            localCommand1.Dispose();
            oConnection.Close();
            oConnection.Dispose();
            objAdapter.Dispose();
            objCommand.Dispose();
            oLocalConnection.Close();
            oLocalConnection.Dispose();
            fn_FillID();
        }
        private void fn_FillID()
        {
            // New connection string for local connection
            string strLocalConnection = System.Configuration.ConfigurationManager.AppSettings["localConnectString"];
            OleDbConnection oLocalConnection = new OleDbConnection(strLocalConnection);
            oLocalConnection.Open();
            OleDbCommand lCommand1 = new OleDbCommand("SELECT Top 1 ID FROM  Invoice_Dec WHERE Uploaded = 1 and New_Car = 'VOID' ORDER BY ID Asc", oLocalConnection);
            intBeginID = Convert.ToInt32(lCommand1.ExecuteScalar().ToString());
            txtBeginID.Text = intBeginID.ToString();
            hidBeginID.Text = intBeginID.ToString();

            OleDbCommand lCommand2 = new OleDbCommand("SELECT Top 1 ID FROM  Invoice_Dec WHERE Uploaded = 1 and New_Car = 'VOID'  ORDER BY ID Desc", oLocalConnection);
            intEndID = Convert.ToInt32(lCommand2.ExecuteScalar().ToString());
            txtEndID.Text = intEndID.ToString();
            hidEndID.Text = intEndID.ToString();

            lCommand1.Dispose();
            lCommand2.Dispose();
            oLocalConnection.Close();
            oLocalConnection.Dispose();
        }

        private void btnVoid_Click_1(object sender, EventArgs e)
        {
            // For void sale in oracle
            // Create connection with local DB
            string strLocalConnection = System.Configuration.ConfigurationManager.AppSettings["localConnectString"];
            OleDbConnection oLocalConnection = new OleDbConnection(strLocalConnection);

            StringBuilder strID = new StringBuilder("select * from ID_Code");
            oLocalConnection.Open();
            OleDbCommand cmdID = new OleDbCommand(strID.ToString(), oLocalConnection);
            OleDbDataReader comprec = cmdID.ExecuteReader();
            comprec.Read();

            int ctr = Convert.ToInt32(comprec["ID"].ToString());
            int c = 1;

            // Create connection with oracle DB
            string strConnection = System.Configuration.ConfigurationManager.AppSettings["oracleConnection"];
            OracleConnection oConnection = new OracleConnection(strConnection);
            string strDelete = "DELETE FROM RA_INTERFACE_LINES_ALL";
            oConnection.Open();
            OracleCommand cmdDelete = new OracleCommand(strDelete, oConnection);
            cmdDelete.ExecuteNonQuery();
            oConnection.Close();

            StringBuilder strLocalQuery = new StringBuilder(" SELECT * FROM Invoice_Dec WHERE ");
            strLocalQuery.Append(" Invoice_No IN (SELECT distinct TRX_Number FROM InvImp) ");
            strLocalQuery.Append(" and id >= '" + txtBeginID.Text + "' ");
            strLocalQuery.Append(" AND id <= '" + txtEndID.Text + "' AND Uploaded = 1 AND New_Car = 'VOID' ");
            //strLocalQuery.Append(" AND Uploaded = 0 AND New_Car = 'VOID' ");
            OleDbCommand cmdLocal = new OleDbCommand(strLocalQuery.ToString(), oLocalConnection);
            OleDbDataReader comprec15 = cmdLocal.ExecuteReader();

            if (comprec15 != null)
            {
                while (comprec15.Read())
                {
                    ctr = ctr + 1;

                    // Code To Map Car No
                    string new_car = "";

                    if (Convert.ToBoolean(comprec15["Owned"].ToString()) == true)
                    {
                        OleDbCommand cmdCar = new OleDbCommand("select * from new_car where vehicle_no = '" + comprec15["vehicle_no"].ToString() + "'", oLocalConnection);
                        OleDbDataReader newcomprec = cmdCar.ExecuteReader();
                        newcomprec.Read();
                        if (newcomprec != null)
                        {
                            new_car = newcomprec["car_no"].ToString();
                        }
                        else
                        {
                            new_car = "0";
                        }
                    }

                    if (Convert.ToBoolean(comprec15["Owned"].ToString()) == false)
                    {
                        new_car = "Vendor Cateogory";
                    }

                    string strcmd = "SELECT distinct(id) FROM auto_Import where RMS_Cust = '" + comprec15["company"].ToString() + "'";
                    cmdID = new OleDbCommand(strcmd, oLocalConnection);
                    OleDbDataReader comprec1 = cmdID.ExecuteReader();
                    comprec1.Read();

                    // Insert record in Oracle database
                    StringBuilder strOraInsert;

                    if (comprec1 != null)
                    {
                        string strInvoice = "SELECT * FROM INVIMP WHERE TRX_NUMBER = '" + comprec15["invoice_no"].ToString() + "'";
                        OleDbCommand cmdInvoice = new OleDbCommand(strInvoice, oLocalConnection);
                        OleDbDataReader comprec2 = cmdInvoice.ExecuteReader();
                        comprec2.Read();

                        if (oConnection.State == ConnectionState.Open)
                            oConnection.Close();
                        oConnection.Open();

                        OracleTransaction oraTransaction = oConnection.BeginTransaction();
                        try
                        {
                            strOraInsert = new StringBuilder(" INSERT INTO RA_INTERFACE_LINES_ALL ");
                            strOraInsert.Append(" (LINE_TYPE, Description, amount,ORIG_SYSTEM_SHIP_CUSTOMER_REF ");
                            strOraInsert.Append(" , ORIG_SYSTEM_SHIP_ADDRESS_REF, ORIG_SYSTEM_BILL_CUSTOMER_REF ");
                            strOraInsert.Append(" , ORIG_SYSTEM_BILL_ADDRESS_REF, RECEIPT_METHOD_ID, CONVERSION_TYPE, CONVERSION_RATE ");
                            strOraInsert.Append(" , TRX_DATE, gl_date, QUANTITY, UNIT_SELLING_PRICE, UNIT_STANDARD_PRICE ");
                            strOraInsert.Append(" , ATTRIBUTE1, UOM_CODE, UOM_NAME, ORG_ID, CREDIT_METHOD_FOR_ACCT_RULE, REFERENCE_LINE_ID ");
                            strOraInsert.Append(" , INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1, INTERFACE_LINE_ATTRIBUTE2 ");
                            strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4, INTERFACE_LINE_ATTRIBUTE5 ");
                            strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7, BATCH_SOURCE_NAME ");
                            strOraInsert.Append(" , SET_OF_BOOKS_ID, CURRENCY_CODE, CUST_TRX_TYPE_NAME, TRX_Number) ");

                            strOraInsert.Append(" VALUES('LINE', 'Standard Package', -" + Convert.ToDecimal(comprec15["basic"].ToString()));
                            strOraInsert.Append(" , " + Convert.ToInt32(comprec1["ID"].ToString()));
                            strOraInsert.Append(" , '" + comprec15["ship_to"].ToString().ToUpper() + "-" + Convert.ToInt32(comprec1["ID"].ToString()) + "' ");
                            strOraInsert.Append(" , " + Convert.ToInt32(comprec1["ID"].ToString()) + ", '" + comprec15["Branch_Booked"].ToString().ToUpper() + "-" + Convert.ToInt32(comprec1["ID"].ToString()) + "' ");
                            strOraInsert.Append(" , " + Convert.ToInt32(comprec15["recipt_id"].ToString()) + ", 'User', 1, TO_DATE('" + comprec15["VDATE"].ToString() + "', 'dd/mm/yyyy:hh:mi:ssam') ");
                            strOraInsert.Append(" , TO_DATE('" + comprec15["VDATE"].ToString() + "', 'dd/mm/yyyy:hh:mi:ssam') , -1, " + Convert.ToDecimal(comprec15["basic"].ToString()));
                            strOraInsert.Append(" , " + Convert.ToDecimal(comprec15["basic"].ToString()) + ", " + ctr + ", 'EA', 'Each', 82, 'PRORATE', " + Convert.ToInt32(comprec2["CUSTOMER_TRX_LINE_ID"].ToString()));
                            strOraInsert.Append(" , 'CIPL Transaction Line', " + ctr + ", 0, 0, 0, 0, 0, 0, 'CIPL Import2' ");
                            strOraInsert.Append(" , 2, 'INR', 'CIPL Import Credit', '" + comprec15["invoice_no"].ToString() + "-CMS' ) ");

                            OracleCommand oraCommand = new OracleCommand(strOraInsert.ToString(), oConnection, oraTransaction);
                            oraCommand.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            oraTransaction.Rollback();
                            continue;
                        }
                        ctr = ctr + 1;
                        int p_Ctr = 0;

                        // --------Line2-------------------------------------------------------
                        if ((Convert.ToInt32(comprec15["Parking_Toll"].ToString()) + Convert.ToInt32(comprec15["other_taxes"].ToString())) > 0)
                        {
                            string strInvoice1 = "SELECT * FROM INVIMP WHERE TRX_NUMBER = '" + comprec15["invoice_no"].ToString() + "' and description = 'Parking & Toll Charges' ";
                            OleDbCommand cmdInvoice1 = new OleDbCommand(strInvoice1, oLocalConnection);
                            OleDbDataReader comprec3 = cmdInvoice1.ExecuteReader();
                            comprec3.Read();

                            try
                            {
                                strOraInsert = new StringBuilder(" INSERT INTO RA_INTERFACE_LINES_ALL (LINE_TYPE, Description");
                                strOraInsert.Append(" , amount, ORIG_SYSTEM_SHIP_CUSTOMER_REF, ORIG_SYSTEM_SHIP_ADDRESS_REF ");
                                strOraInsert.Append(" , ORIG_SYSTEM_BILL_CUSTOMER_REF, ORIG_SYSTEM_BILL_ADDRESS_REF, RECEIPT_METHOD_ID ");
                                strOraInsert.Append(" , CONVERSION_TYPE, CONVERSION_RATE, TRX_DATE, gl_date, QUANTITY, UNIT_SELLING_PRICE ");
                                strOraInsert.Append(" , UNIT_STANDARD_PRICE, ATTRIBUTE1, UOM_CODE, UOM_NAME, ORG_ID ");
                                strOraInsert.Append(" , INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1 ");
                                strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE2, INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4 ");
                                strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE5, INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7 ");
                                strOraInsert.Append(" , BATCH_SOURCE_NAME, SET_OF_BOOKS_ID, CURRENCY_CODE, CUST_TRX_TYPE_NAME, TRX_Number ");
                                strOraInsert.Append(" , CREDIT_METHOD_FOR_ACCT_RULE, REFERENCE_LINE_ID) ");

                                strOraInsert.Append(" VALUES('LINE', 'Parking & Toll Charges', -" + (Convert.ToInt32(comprec15["Parking_Toll"].ToString()) + Convert.ToInt32(comprec15["other_taxes"].ToString())));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec1["ID"].ToString()) + ", '" + comprec15["ship_to"].ToString().ToUpper() + "-" + Convert.ToInt32(comprec1["ID"].ToString()) + "' ");
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec1["ID"].ToString()) + ", '" + comprec15["Branch_Booked"].ToString().ToUpper() + "-" + Convert.ToInt32(comprec1["ID"].ToString()) + "' ");
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["recipt_id"].ToString()) + ", 'User', 1, TO_DATE('" + comprec15["VDATE"].ToString() + "', 'dd/mm/yyyy:hh:mi:ssam'), TO_DATE('" + comprec15["VDATE"].ToString() + "', 'dd/mm/yyyy:hh:mi:ssam') ");
                                strOraInsert.Append(" , -1, " + (Convert.ToInt32(comprec15["Parking_Toll"].ToString()) + Convert.ToInt32(comprec15["other_taxes"].ToString())));
                                strOraInsert.Append(" , " + (Convert.ToInt32(comprec15["Parking_Toll"].ToString()) + Convert.ToInt32(comprec15["other_taxes"].ToString())));
                                strOraInsert.Append(" , " + ctr + ", 'EA', 'Each', 82 ");
                                strOraInsert.Append(" , 'CIPL Transaction Line', " + ctr + ", 0, 0, 0, 0, 0, 0, 'CIPL Import2' ");
                                strOraInsert.Append(" , 2, 'INR', 'CIPL Import Credit', '" + comprec15["invoice_no"].ToString() + "-CMP' ");
                                strOraInsert.Append(" , 'PRORATE', " + Convert.ToInt32(comprec3["CUSTOMER_TRX_LINE_ID"].ToString()) + " ) ");

                                p_Ctr = ctr;

                                OracleCommand oraCommand2 = new OracleCommand(strOraInsert.ToString(), oConnection, oraTransaction);
                                oraCommand2.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                oraTransaction.Rollback();
                                continue;
                            }
                        }
                        oraTransaction.Commit();
                        oraTransaction.Dispose();

                    }// End of comprec1 record set

                }// End of while loop
            }// End of Main IF

            StringBuilder strUpdate = new StringBuilder(" UPDATE id_code set id = " + ctr);
            OleDbCommand cmdUpdate = new OleDbCommand(strUpdate.ToString(), oLocalConnection);
            cmdUpdate.ExecuteNonQuery();

            //MessageBox.Show("Total " + inv_ctr + " records are uploaded");
            oConnection.Close();
            oLocalConnection.Close();
            cmdDelete.Dispose();
            cmdID.Dispose();
            cmdLocal.Dispose();
            cmdUpdate.Dispose();
            comprec.Dispose();
            comprec15.Dispose();
        }

        private void btnUpdateInvoice_Click(object sender, EventArgs e)
        {
            // For updating uploaded column value
            // Create connection with oracle DB
            string strConnection = System.Configuration.ConfigurationManager.AppSettings["oracleConnection"];
            OracleConnection oConnection = new OracleConnection(strConnection);
            string strOraQuery = "SELECT distinct substr(TRX_Number,0,length(TRX_NUMBER)-4) as TRX_Number FROM RA_INTERFACE_LINES_ALL WHERE Interface_Status = 'P'";
            oConnection.Open();
            OracleCommand cmdOraQuery = new OracleCommand(strOraQuery, oConnection);
            OracleDataReader comprec = cmdOraQuery.ExecuteReader();
            int intVal = 0;
            // Create connection with local DB
            string strLocalConnection = System.Configuration.ConfigurationManager.AppSettings["localConnectString"];
            OleDbConnection oLocalConnection = new OleDbConnection(strLocalConnection);

            if (comprec != null)
            {
                string strUpdateQuery = "";
                while (comprec.Read())
                {
                    strUpdateQuery = "UPDATE Invoice_Dec SET Uploaded = 1 WHERE Invoice_No = '" +  comprec["TRX_Number"] + "' ";
                    if (oLocalConnection.State == ConnectionState.Open)
                        oLocalConnection.Close();
                    oLocalConnection.Open();
                    OleDbCommand cmdUpdate = new OleDbCommand(strUpdateQuery, oLocalConnection);
                    cmdUpdate.ExecuteNonQuery();
                    intVal++;
                }
            }
            oLocalConnection.Close();
            comprec.Dispose();
            cmdOraQuery.Dispose();
            oConnection.Close();
            MessageBox.Show("Total " + (intVal - 1) + " Invoices updated");
        }
    }
}