namespace OracleInterface
{
    partial class frmDownLoad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label8 = new System.Windows.Forms.Label();
            this.pnlMessage = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.lblUpTime = new System.Windows.Forms.Label();
            this.lblDownTime = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnUploadOracle = new System.Windows.Forms.Button();
            this.btnOn = new System.Windows.Forms.Button();
            this.btnOff = new System.Windows.Forms.Button();
            this.btnUpdateInvoice = new System.Windows.Forms.Button();
            this.textStatus = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.lblSdld = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tmrDwnld = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tmrUpload = new System.Windows.Forms.Timer(this.components);
            this.pnlMessage.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(102, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "For Downloading >>";
            // 
            // pnlMessage
            // 
            this.pnlMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMessage.Controls.Add(this.label10);
            this.pnlMessage.Controls.Add(this.lblUpTime);
            this.pnlMessage.Controls.Add(this.lblDownTime);
            this.pnlMessage.Controls.Add(this.label9);
            this.pnlMessage.Controls.Add(this.label8);
            this.pnlMessage.Location = new System.Drawing.Point(16, 109);
            this.pnlMessage.Name = "pnlMessage";
            this.pnlMessage.Size = new System.Drawing.Size(162, 100);
            this.pnlMessage.TabIndex = 30;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.SystemColors.ControlText;
            this.label10.ForeColor = System.Drawing.Color.Lime;
            this.label10.Location = new System.Drawing.Point(-1, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(160, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Data Transfer is Scheduled As :-";
            // 
            // lblUpTime
            // 
            this.lblUpTime.AutoSize = true;
            this.lblUpTime.Location = new System.Drawing.Point(111, 62);
            this.lblUpTime.Name = "lblUpTime";
            this.lblUpTime.Size = new System.Drawing.Size(41, 13);
            this.lblUpTime.TabIndex = 20;
            this.lblUpTime.Text = "label11";
            // 
            // lblDownTime
            // 
            this.lblDownTime.AutoSize = true;
            this.lblDownTime.Location = new System.Drawing.Point(111, 38);
            this.lblDownTime.Name = "lblDownTime";
            this.lblDownTime.Size = new System.Drawing.Size(41, 13);
            this.lblDownTime.TabIndex = 19;
            this.lblDownTime.Text = "label10";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 62);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "For Uploading >>";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Maroon;
            this.label7.Location = new System.Drawing.Point(469, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Current Time :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(469, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Current Hour :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(469, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "Today\'s Date :";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnUploadOracle);
            this.panel1.Controls.Add(this.btnOn);
            this.panel1.Controls.Add(this.btnOff);
            this.panel1.Controls.Add(this.btnUpdateInvoice);
            this.panel1.Location = new System.Drawing.Point(262, 109);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(293, 100);
            this.panel1.TabIndex = 26;
            // 
            // btnUploadOracle
            // 
            this.btnUploadOracle.Enabled = false;
            this.btnUploadOracle.ForeColor = System.Drawing.Color.Maroon;
            this.btnUploadOracle.Location = new System.Drawing.Point(167, 62);
            this.btnUploadOracle.Name = "btnUploadOracle";
            this.btnUploadOracle.Size = new System.Drawing.Size(107, 23);
            this.btnUploadOracle.TabIndex = 12;
            this.btnUploadOracle.Text = "Upload in ORACLE";
            this.btnUploadOracle.UseVisualStyleBackColor = true;
            this.btnUploadOracle.Click += new System.EventHandler(this.btnUploadOracle_Click);
            // 
            // btnOn
            // 
            this.btnOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnOn.Enabled = false;
            this.btnOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOn.ForeColor = System.Drawing.Color.Black;
            this.btnOn.Location = new System.Drawing.Point(37, 15);
            this.btnOn.Name = "btnOn";
            this.btnOn.Size = new System.Drawing.Size(107, 23);
            this.btnOn.TabIndex = 1;
            this.btnOn.Text = "Start";
            this.btnOn.UseVisualStyleBackColor = false;
            this.btnOn.Click += new System.EventHandler(this.btnOn_Click);
            // 
            // btnOff
            // 
            this.btnOff.Enabled = false;
            this.btnOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOff.ForeColor = System.Drawing.Color.Black;
            this.btnOff.Location = new System.Drawing.Point(167, 15);
            this.btnOff.Name = "btnOff";
            this.btnOff.Size = new System.Drawing.Size(107, 23);
            this.btnOff.TabIndex = 2;
            this.btnOff.Text = "Stop";
            this.btnOff.UseVisualStyleBackColor = true;
            this.btnOff.Click += new System.EventHandler(this.btnOff_Click);
            // 
            // btnUpdateInvoice
            // 
            this.btnUpdateInvoice.Enabled = false;
            this.btnUpdateInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateInvoice.ForeColor = System.Drawing.Color.Red;
            this.btnUpdateInvoice.Location = new System.Drawing.Point(37, 62);
            this.btnUpdateInvoice.Name = "btnUpdateInvoice";
            this.btnUpdateInvoice.Size = new System.Drawing.Size(107, 23);
            this.btnUpdateInvoice.TabIndex = 11;
            this.btnUpdateInvoice.Text = "Update Invoice";
            this.btnUpdateInvoice.UseVisualStyleBackColor = true;
            this.btnUpdateInvoice.Click += new System.EventHandler(this.btnUpdateInvoice_Click);
            // 
            // textStatus
            // 
            this.textStatus.BackColor = System.Drawing.SystemColors.MenuText;
            this.textStatus.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textStatus.ForeColor = System.Drawing.Color.White;
            this.textStatus.Location = new System.Drawing.Point(233, 229);
            this.textStatus.Name = "textStatus";
            this.textStatus.ReadOnly = true;
            this.textStatus.Size = new System.Drawing.Size(502, 21);
            this.textStatus.TabIndex = 25;
            this.textStatus.Text = "Please click on \"START\" button to start with";
            this.textStatus.TextChanged += new System.EventHandler(this.textStatus_TextChanged);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // txtStatus
            // 
            this.txtStatus.BackColor = System.Drawing.SystemColors.MenuText;
            this.txtStatus.ForeColor = System.Drawing.Color.Lime;
            this.txtStatus.Location = new System.Drawing.Point(7, 229);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.Size = new System.Drawing.Size(229, 20);
            this.txtStatus.TabIndex = 24;
            // 
            // lblSdld
            // 
            this.lblSdld.AutoSize = true;
            this.lblSdld.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSdld.ForeColor = System.Drawing.Color.Maroon;
            this.lblSdld.Location = new System.Drawing.Point(4, 23);
            this.lblSdld.Name = "lblSdld";
            this.lblSdld.Size = new System.Drawing.Size(236, 16);
            this.lblSdld.TabIndex = 23;
            this.lblSdld.Text = "Automatic Data Transfer Is  Scheduled";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(565, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Hour";
            // 
            // tmrDwnld
            // 
            this.tmrDwnld.Interval = 4000;
            this.tmrDwnld.Tick += new System.EventHandler(this.tmrDwnld_Tick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(565, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Time";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(565, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Location = new System.Drawing.Point(154, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(301, 17);
            this.label1.TabIndex = 19;
            this.label1.Text = "Oracle Interface - Data Transfer Service";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // tmrUpload
            // 
            this.tmrUpload.Interval = 3000;
            this.tmrUpload.Tick += new System.EventHandler(this.tmrUpload_Tick);
            // 
            // frmDownLoad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(737, 251);
            this.Controls.Add(this.pnlMessage);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textStatus);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.lblSdld);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "frmDownLoad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Automatic Data Transfer  :: Internal software";
            this.Load += new System.EventHandler(this.frmDownLoad_Load);
            this.pnlMessage.ResumeLayout(false);
            this.pnlMessage.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel pnlMessage;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblUpTime;
        private System.Windows.Forms.Label lblDownTime;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnOn;
        private System.Windows.Forms.Button btnOff;
        private System.Windows.Forms.Button btnUpdateInvoice;
        private System.Windows.Forms.TextBox textStatus;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.Label lblSdld;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer tmrDwnld;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUploadOracle;
        private System.Windows.Forms.Timer tmrUpload;
    }
}