namespace OracleInterface
{
    partial class frmIndex
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpControl = new System.Windows.Forms.GroupBox();
            this.btnSaleVoid = new System.Windows.Forms.Button();
            this.btnSaleUpdate = new System.Windows.Forms.Button();
            this.grpControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpControl
            // 
            this.grpControl.Controls.Add(this.btnSaleVoid);
            this.grpControl.Controls.Add(this.btnSaleUpdate);
            this.grpControl.Location = new System.Drawing.Point(142, 70);
            this.grpControl.Name = "grpControl";
            this.grpControl.Size = new System.Drawing.Size(216, 99);
            this.grpControl.TabIndex = 0;
            this.grpControl.TabStop = false;
            // 
            // btnSaleVoid
            // 
            this.btnSaleVoid.Location = new System.Drawing.Point(107, 40);
            this.btnSaleVoid.Name = "btnSaleVoid";
            this.btnSaleVoid.Size = new System.Drawing.Size(102, 23);
            this.btnSaleVoid.TabIndex = 1;
            this.btnSaleVoid.Text = "Void Sale";
            this.btnSaleVoid.UseVisualStyleBackColor = true;
            this.btnSaleVoid.Click += new System.EventHandler(this.btnSaleVoid_Click);
            // 
            // btnSaleUpdate
            // 
            this.btnSaleUpdate.Location = new System.Drawing.Point(6, 40);
            this.btnSaleUpdate.Name = "btnSaleUpdate";
            this.btnSaleUpdate.Size = new System.Drawing.Size(102, 23);
            this.btnSaleUpdate.TabIndex = 0;
            this.btnSaleUpdate.Text = "Upload Revenue";
            this.btnSaleUpdate.UseVisualStyleBackColor = true;
            this.btnSaleUpdate.Click += new System.EventHandler(this.btnSaleUpdate_Click);
            // 
            // frmIndex
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 264);
            this.Controls.Add(this.grpControl);
            this.MaximizeBox = false;
            this.Name = "frmIndex";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmIndex";
            this.Load += new System.EventHandler(this.frmIndex_Load);
            this.grpControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpControl;
        private System.Windows.Forms.Button btnSaleVoid;
        private System.Windows.Forms.Button btnSaleUpdate;
    }
}