namespace OracleInterface
{
    partial class frmCancel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnQuery = new System.Windows.Forms.Button();
            this.toDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.fromDate = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboLocation = new System.Windows.Forms.ComboBox();
            this.hidEndID = new System.Windows.Forms.TextBox();
            this.hidBeginID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEndID = new System.Windows.Forms.TextBox();
            this.lblMsg = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlUpload = new System.Windows.Forms.Panel();
            this.btnUpdateInvoice = new System.Windows.Forms.Button();
            this.btnVoid = new System.Windows.Forms.Button();
            this.txtBeginID = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.pnlUpload.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(92, 97);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(99, 23);
            this.btnQuery.TabIndex = 7;
            this.btnQuery.Text = "Submit";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // toDate
            // 
            this.toDate.CustomFormat = "MM/dd/yyyy";
            this.toDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.toDate.Location = new System.Drawing.Point(256, 65);
            this.toDate.Name = "toDate";
            this.toDate.Size = new System.Drawing.Size(86, 20);
            this.toDate.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Latha", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(194, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 14);
            this.label3.TabIndex = 5;
            this.label3.Text = "To Date";
            // 
            // fromDate
            // 
            this.fromDate.CustomFormat = "MM/dd/yyyy";
            this.fromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.fromDate.Location = new System.Drawing.Point(92, 61);
            this.fromDate.Name = "fromDate";
            this.fromDate.Size = new System.Drawing.Size(86, 20);
            this.fromDate.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnQuery);
            this.groupBox1.Controls.Add(this.toDate);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.fromDate);
            this.groupBox1.Controls.Add(this.cboLocation);
            this.groupBox1.Location = new System.Drawing.Point(89, 52);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(362, 137);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Latha", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(30, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 14);
            this.label2.TabIndex = 4;
            this.label2.Text = "From Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Latha", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 14);
            this.label1.TabIndex = 3;
            this.label1.Text = "Location";
            // 
            // cboLocation
            // 
            this.cboLocation.FormattingEnabled = true;
            this.cboLocation.Location = new System.Drawing.Point(92, 19);
            this.cboLocation.Name = "cboLocation";
            this.cboLocation.Size = new System.Drawing.Size(143, 21);
            this.cboLocation.TabIndex = 0;
            // 
            // hidEndID
            // 
            this.hidEndID.Location = new System.Drawing.Point(32, 143);
            this.hidEndID.Name = "hidEndID";
            this.hidEndID.Size = new System.Drawing.Size(20, 20);
            this.hidEndID.TabIndex = 19;
            this.hidEndID.Visible = false;
            // 
            // hidBeginID
            // 
            this.hidBeginID.Location = new System.Drawing.Point(3, 143);
            this.hidBeginID.Name = "hidBeginID";
            this.hidBeginID.Size = new System.Drawing.Size(23, 20);
            this.hidBeginID.TabIndex = 18;
            this.hidBeginID.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(179, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "ID To";
            // 
            // txtEndID
            // 
            this.txtEndID.Location = new System.Drawing.Point(219, 51);
            this.txtEndID.Name = "txtEndID";
            this.txtEndID.Size = new System.Drawing.Size(43, 20);
            this.txtEndID.TabIndex = 14;
            // 
            // lblMsg
            // 
            this.lblMsg.AutoSize = true;
            this.lblMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.Location = new System.Drawing.Point(3, 10);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(231, 15);
            this.lblMsg.TabIndex = 17;
            this.lblMsg.Text = "Select ID for reverse data in Oracle";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(80, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "ID From";
            // 
            // pnlUpload
            // 
            this.pnlUpload.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlUpload.Controls.Add(this.btnUpdateInvoice);
            this.pnlUpload.Controls.Add(this.btnVoid);
            this.pnlUpload.Controls.Add(this.hidEndID);
            this.pnlUpload.Controls.Add(this.hidBeginID);
            this.pnlUpload.Controls.Add(this.lblMsg);
            this.pnlUpload.Controls.Add(this.label5);
            this.pnlUpload.Controls.Add(this.txtEndID);
            this.pnlUpload.Controls.Add(this.label4);
            this.pnlUpload.Controls.Add(this.txtBeginID);
            this.pnlUpload.Location = new System.Drawing.Point(89, 195);
            this.pnlUpload.Name = "pnlUpload";
            this.pnlUpload.Size = new System.Drawing.Size(362, 166);
            this.pnlUpload.TabIndex = 15;
            // 
            // btnUpdateInvoice
            // 
            this.btnUpdateInvoice.Location = new System.Drawing.Point(196, 102);
            this.btnUpdateInvoice.Name = "btnUpdateInvoice";
            this.btnUpdateInvoice.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateInvoice.TabIndex = 21;
            this.btnUpdateInvoice.Text = "Update Invoice";
            this.btnUpdateInvoice.UseVisualStyleBackColor = true;
            this.btnUpdateInvoice.Click += new System.EventHandler(this.btnUpdateInvoice_Click);
            // 
            // btnVoid
            // 
            this.btnVoid.Location = new System.Drawing.Point(115, 102);
            this.btnVoid.Name = "btnVoid";
            this.btnVoid.Size = new System.Drawing.Size(75, 23);
            this.btnVoid.TabIndex = 20;
            this.btnVoid.Text = "Cancel Sale";
            this.btnVoid.UseVisualStyleBackColor = true;
            this.btnVoid.Click += new System.EventHandler(this.btnVoid_Click_1);
            // 
            // txtBeginID
            // 
            this.txtBeginID.Location = new System.Drawing.Point(130, 51);
            this.txtBeginID.Name = "txtBeginID";
            this.txtBeginID.Size = new System.Drawing.Size(43, 20);
            this.txtBeginID.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(84, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(294, 25);
            this.label6.TabIndex = 16;
            this.label6.Text = "Oracle Interface - Reverse Sales";
            // 
            // frmCancel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 377);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pnlUpload);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.Name = "frmCancel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmCancel";
            this.Load += new System.EventHandler(this.frmCancel_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pnlUpload.ResumeLayout(false);
            this.pnlUpload.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.DateTimePicker toDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker fromDate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboLocation;
        private System.Windows.Forms.TextBox hidEndID;
        private System.Windows.Forms.TextBox hidBeginID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtEndID;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel pnlUpload;
        private System.Windows.Forms.TextBox txtBeginID;
        private System.Windows.Forms.Button btnVoid;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnUpdateInvoice;
    }
}