using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OracleInterface
{
    public partial class frmIndex : Form
    {
        public frmIndex()
        {
            InitializeComponent();
        }

        private void btnSaleUpdate_Click(object sender, EventArgs e)
        {
            frmData objData = new frmData();
            objData.ShowDialog();
        }

        private void btnSaleVoid_Click(object sender, EventArgs e)
        {
            frmCancel objCancel = new frmCancel();
            objCancel.ShowDialog();
        }

        private void frmIndex_Load(object sender, EventArgs e)
        {

        }
    }
}