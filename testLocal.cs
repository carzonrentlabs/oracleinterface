using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace OracleInterface
{
    public partial class testLocal : Form
    {
        public testLocal()
        {
            InitializeComponent();
        }

        private void testLocal_Load(object sender, EventArgs e)
        {
            string strConnection = System.Configuration.ConfigurationManager.AppSettings["localConnectString"];
            StringBuilder strQuery;
            OleDbConnection oConnection = new OleDbConnection(strConnection);
            oConnection.Open();

            strQuery = new StringBuilder(" SELECT id, BookingID, Status ");
            strQuery.Append("  FROM   demo ");
            //strQuery.Append("  WHERE Status = 0 ");

            OleDbCommand objCommand = new OleDbCommand(strQuery.ToString(), oConnection);
            OleDbDataAdapter objAdapter = new OleDbDataAdapter(objCommand);
            DataSet objDataset = new DataSet();
            objAdapter.Fill(objDataset, "demo");

            dataGridView1.DataSource = objDataset.Tables[0];
            objDataset.Tables[0].Rows[1][2] = 1;
            objDataset.Tables[0].Rows[1].AcceptChanges();

            //dataGridView1.DataSource = objDataset.Tables[0];

            try
            {
                OleDbCommandBuilder objCommBilder = new OleDbCommandBuilder(objAdapter);
                //objAdapter.UpdateCommand = objCommBilder.GetUpdateCommand();
                objAdapter.Update(objDataset, "demo");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            oConnection.Close();
        }
    }
}