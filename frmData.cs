using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.OracleClient;

namespace OracleInterface
{
    public partial class frmData : Form
    {
        int intBeginID=0;
        int intEndID=0;

        public string strTableName = "Invoice_DEC_test";
        public frmData()
        {
            InitializeComponent();
        }

        long IDlocationnew;
        long IDproductnew;
        private void get_ID()
        {
            // New connection string for local connection
            string strLocalConnection = System.Configuration.ConfigurationManager.AppSettings["localConnectString"];
            OleDbConnection oLocalConnection = new OleDbConnection(strLocalConnection);
            oLocalConnection.Open();
            OleDbCommand lCommand1 = new OleDbCommand("SELECT Top 1 ID FROM  " + strTableName + " WHERE Uploaded = 0 and New_Car <> 'Void' ORDER BY ID Asc", oLocalConnection);
            intBeginID = Convert.ToInt32(lCommand1.ExecuteScalar().ToString());

            OleDbCommand lCommand2 = new OleDbCommand("SELECT Top 1 ID FROM  " + strTableName + " WHERE Uploaded = 0 and New_Car <> 'Void' ORDER BY ID Desc", oLocalConnection);
            intEndID = Convert.ToInt32(lCommand2.ExecuteScalar().ToString());

            lCommand1.Dispose();
            lCommand2.Dispose();
            oLocalConnection.Close();
            oLocalConnection.Dispose();
        }
        private void frmData_Load(object sender, EventArgs e)
        {
            get_ID();
            hidBeginID.Text  = intBeginID.ToString();
            hidEndID.Text  = intEndID.ToString();
            /*string strConnection = System.Configuration.ConfigurationManager.AppSettings["corConnectString"];
            string strQuery;
            SqlConnection oConnection = new SqlConnection(strConnection);

            strQuery = " SELECT CityID, CityName FROM CORIntCityMaster WHERE Active = 1";
            SqlCommand objCommand = new SqlCommand(strQuery.ToString(), oConnection);
            SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
            DataSet objDataset = new DataSet();
            objAdapter.Fill(objDataset);
            cboLocation.DataSource = objDataset.Tables[0];
            cboLocation.DisplayMember = "CityName";
            cboLocation.ValueMember = "CityID";
            //cboLocation.Items.Add(objDataset.Tables[0].Columns[0]);
            objCommand.Dispose();
            objAdapter.Dispose();
            objDataset.Dispose();
            oConnection.Close();
            oConnection.Dispose();*/
        }

        /*private void button1_Click(object sender, EventArgs e)
        {
            // Connection from Insta-Server
            string strConnection = System.Configuration.ConfigurationManager.AppSettings["corConnectString"];
            StringBuilder strQuery;
           

            SqlConnection oConnection = new SqlConnection(strConnection);

            string strLocation = cboLocation.SelectedValue.ToString();
            string strFromDate = fromDate.Text;
            string strToDate = toDate.Text;
           

            strQuery = new StringBuilder(" SELECT 1 AS ID ");
            strQuery.Append(" , CASE f.Company WHEN 'IBM India Pvt. Ltd.' THEN (Select Ship_To_1 FROM tblInterface WHERE Ship_To = f.PickUpCity) ");
            strQuery.Append(" WHEN 'Nokia India Pvt. Ltd.--SEZ COR Chennai' THEN (Select Ship_To_1 FROM tblInterface WHERE Ship_To = f.PickUpCity) ");
            strQuery.Append(" WHEN 'Nokia India Pvt. Ltd.' THEN (Select Ship_To_1 FROM tblInterface WHERE Ship_To = f.PickUpCity) ");
            strQuery.Append(" WHEN 'Oracle India Pvt. Ltd.' THEN (Select Ship_To_1 FROM tblInterface WHERE Ship_To = f.PickUpCity) ");
            strQuery.Append(" WHEN 'Carzonrent India Pvt. Ltd. COR Delhi' THEN (Select Ship_To_1 FROM tblInterface WHERE Ship_To = f.PickUpCity) ");
            strQuery.Append(" WHEN 'Cisco Systems' THEN (Select Ship_To_1 FROM tblInterface WHERE Ship_To = f.PickUpCity) ");
            strQuery.Append(" WHEN 'Walk in' THEN (Select Ship_To_1 FROM tblInterface WHERE Ship_To = f.PickUpCity) ");
            strQuery.Append(" ELSE t.name END as Branch_Bill_To ");
            strQuery.Append(" , f.Company as Company, f.ClientName as Client_Name, ");
            strQuery.Append(" (f.OutStnAmt + f.FuelSurcharge + f.NightStayAmt + f.OtherCharges + f.BasicPkgRate + f.ExtraHrsRate ");
            strQuery.Append("  + f.ExtraKMRate + f.AdjustedAmount) as Basic, f.ParkTollCharges as Parking_Toll, F.InterstateTax as Other_Taxes ");
            strQuery.Append("  , VehicleNo as Vehicle_No, CONVERT(VARCHAR(10),  f.DATE_IN, 101) as Vdate, InvoiceNo as Actual_Invoice, CONVERT(VARCHAR(10), DATE_OUT, 101) as Car_Used_Date ");
            strQuery.Append("  , InvoiceNo as Invoice_No, DutySlipNo as Duty_Slip_No, 0 as STax, '' as Car_Booked_by ");
            strQuery.Append(" , CASE f.Company WHEN 'IBM India Pvt. Ltd.' THEN PickUpCity ");
            strQuery.Append(" WHEN 'Nokia India Pvt. Ltd.--SEZ COR Chennai' THEN PickUpCity ");
            strQuery.Append(" WHEN 'Nokia India Pvt. Ltd.' THEN PickUpCity ");
            strQuery.Append(" WHEN 'Oracle India Pvt. Ltd.' THEN PickUpCity ");
            strQuery.Append(" WHEN 'Carzonrent India Pvt. Ltd. COR Delhi' THEN PickUpCity ");
            strQuery.Append(" WHEN 'Cisco Systems' THEN PickUpCity ");
            strQuery.Append(" WHEN 'Walk in' THEN PickUpCity ");
            strQuery.Append(" ELSE t.Branch END as Branch_Booked ");
            strQuery.Append("  , (CASE f.ClientType WHEN 'Cr' Then 0 Else 1 End) as Direct, CarBooked as Cat ");
            strQuery.Append("  , (CASE f.VendorName WHEN 'Own Car' Then 1 Else 0 End) as Owned, CategoryBooked as Category ");
            strQuery.Append("  , KMOut as KM_OUT, KMIn as KM_in, VendorName as Reason_Delay, '' as Reason_Cancel, OutstationYesNo as Outstation ");
            strQuery.Append("  , ExtraKM as Extra_Km, ExtraHr as Extra_Hrs, OutStnAmt as Outstation_Amt, FuelSurcharge as Fuel ");
            strQuery.Append("  , NightStayAmt as Night, OtherCharges as Other_Charges, t.Product as Product ");
            strQuery.Append("  , 0 as Vat, PkgName as Package_Name, '' as Driver, '' as Assignment, PickUpAdd as Pickup ");
            strQuery.Append("  , PickUpCity as CITY_NAME, '' as Remark, PickUpCity as SHIP_TO, '' as INVOICE_TYPE, '' as New_Car ");
            strQuery.Append("  , 0 as Uploaded, (Select Ship_To_1 FROM tblInterface WHERE Ship_To = f.PickUpCity) as Ship_to1, 5 as Term_Id, 1010 as Recipt_ID, ServiceTaxPercent as Service_Tax ");
            strQuery.Append("  , EduCessPercent as Cess_Tax, DSTPercent as Vat_Tax ");
            strQuery.Append("  FROM   fn_DataForOracleSale_New_Monthly('" + strFromDate +"', '"+ strToDate +"', "+ strLocation +") as f ");
            strQuery.Append("  , tblInterface as t ");
            strQuery.Append("  WHERE f.CompanyCityID = t.CityID ");
            strQuery.ToString();

            SqlCommand objCommand = new SqlCommand(strQuery.ToString(), oConnection);
            objCommand.CommandTimeout = 1800;
            SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
            DataSet objDataset = new DataSet();
            objAdapter.Fill(objDataset);

            objCommand.Dispose();
            objAdapter.Dispose();
            oConnection.Close();

            // New connection string for local connection
            string strLocalConnection = System.Configuration.ConfigurationManager.AppSettings["localConnectString"];
            OleDbConnection oLocalConnection = new OleDbConnection(strLocalConnection);
            oLocalConnection.Open();
            OleDbCommand localCommand;
            
            StringBuilder strInsert;
            for (int i = 0; i < objDataset.Tables[0].Rows.Count; i++)
            {
               //  decimal decVatTax = 0;
               //  if (objDataset.Tables[0].Rows[i][48].ToString() != "")
               //  {
               //     decVatTax = Convert.ToDecimal(objDataset.Tables[0].Rows[i][48]);
               //  }

                decimal decCess = (Convert.ToDecimal(objDataset.Tables[0].Rows[i][46]) * Convert.ToDecimal(objDataset.Tables[0].Rows[i][47])) / 100;
                decimal decServiceTax = Convert.ToDecimal(objDataset.Tables[0].Rows[i][46]) + decCess;
                decimal decSTax = ((Convert.ToDecimal(objDataset.Tables[0].Rows[i][4]) + Convert.ToDecimal(objDataset.Tables[0].Rows[i][5]) + Convert.ToDecimal(objDataset.Tables[0].Rows[i][6])) * decServiceTax) / 100;
                decimal decVat = ((Convert.ToDecimal(objDataset.Tables[0].Rows[i][4]) + Convert.ToDecimal(objDataset.Tables[0].Rows[i][5]) + Convert.ToDecimal(objDataset.Tables[0].Rows[i][6])) * Convert.ToDecimal(objDataset.Tables[0].Rows[i][48])) / 100;
               
                strInsert = new StringBuilder(" INSERT INTO " + strTableName + " (Branch_Bill_To, Company, Client_Name, Basic ");
                strInsert.Append(" , Parking_Toll, Other_Taxes, Vehicle_No, Vdate, Actual_Invoice ");
                strInsert.Append(" , Car_Used_Date, Invoice_No, Duty_Slip_No, STax, Car_Booked_by ");
                strInsert.Append(" , Branch_Booked, Direct, Cat, Owned, Category, KM_OUT, KM_in ");
                strInsert.Append(" , Reason_Delay, Reason_Cancel, Outstation, Extra_Km, Extra_Hrs ");
                strInsert.Append(" , Outstation_Amt, Fuel, Night, Other_Charges, Product, Vat ");
                strInsert.Append(" , Package_Name, Driver, Assignment, Pickup, CITY_NAME, Remark ");
                strInsert.Append(" , SHIP_TO, INVOICE_TYPE, New_Car, Ship_to1, Term_Id, Recipt_ID ");
                strInsert.Append(" , Service_Tax, Cess_Tax, Vat_Tax) ");
                strInsert.Append(" VALUES ('" + objDataset.Tables[0].Rows[i][1].ToString() + "'");
               
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][2].ToString().Replace("'","''")  + "' ");
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][3].ToString().Replace("'", "''") + "'");
                strInsert.Append(" , " + Convert.ToDecimal(objDataset.Tables[0].Rows[i][4]) );
                strInsert.Append(" , " + Convert.ToDecimal(objDataset.Tables[0].Rows[i][5]) );
                strInsert.Append(" , " + Convert.ToDecimal(objDataset.Tables[0].Rows[i][6]) );
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][7].ToString() + "'");
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][8].ToString() + "'");
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][9].ToString() + "'");
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][10].ToString() + "'");
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][11].ToString() + "'");
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][12].ToString() + "'");
                strInsert.Append(" , " + Math.Round(decSTax,2));
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][14].ToString() + "'");
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][15].ToString() + "'");
                strInsert.Append(" , " + objDataset.Tables[0].Rows[i][16] );
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][17].ToString() + "'");
                strInsert.Append(" , " + objDataset.Tables[0].Rows[i][18]);
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][19].ToString() + "'");
                strInsert.Append(" , " + Convert.ToDecimal(objDataset.Tables[0].Rows[i][20]));
                strInsert.Append(" , " + Convert.ToDecimal(objDataset.Tables[0].Rows[i][21]));
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][22].ToString().Replace("'", "''") + "'");
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][23].ToString().Replace("'", "''") + "'");
                strInsert.Append(" , " + objDataset.Tables[0].Rows[i][24].ToString() );
                strInsert.Append(" , " + Convert.ToDecimal(objDataset.Tables[0].Rows[i][25]));
                strInsert.Append(" , " + Convert.ToDecimal(objDataset.Tables[0].Rows[i][26]));
                strInsert.Append(" , " + Convert.ToDecimal(objDataset.Tables[0].Rows[i][27]));
                strInsert.Append(" , " + Convert.ToDecimal(objDataset.Tables[0].Rows[i][28]));
                strInsert.Append(" , " + Convert.ToDecimal(objDataset.Tables[0].Rows[i][29]));
                strInsert.Append(" , " + Convert.ToDecimal(objDataset.Tables[0].Rows[i][30]));
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][31].ToString() + "'");
                strInsert.Append(" , " + Math.Round(decVat,2));
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][33].ToString() + "'");
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][34].ToString() + "'");
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][35].ToString() + "'");
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][36].ToString().Replace("'", "''") + "'");
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][37].ToString() + "'");
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][38].ToString() + "'");
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][39].ToString() + "'");
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][40].ToString() + "'");
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][41].ToString() + "'");
                strInsert.Append(" , '" + objDataset.Tables[0].Rows[i][43].ToString() + "'");
                strInsert.Append(" , " + Convert.ToDecimal(objDataset.Tables[0].Rows[i][44]));
                strInsert.Append(" , " + Convert.ToDecimal(objDataset.Tables[0].Rows[i][45]));
                strInsert.Append(" , " + Convert.ToDecimal(objDataset.Tables[0].Rows[i][46]));
                strInsert.Append(" , " + Math.Round(decCess,2));
                strInsert.Append(" , " + Convert.ToDecimal(objDataset.Tables[0].Rows[i][48]) + ")");

                localCommand = new OleDbCommand(strInsert.ToString(), oLocalConnection);
                localCommand.CommandTimeout = 1800;
                localCommand.ExecuteNonQuery();
            }
            oLocalConnection.Close();
            oLocalConnection.Dispose();

            // Loop for Updateing OraStatus in CORIntInvoice table
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();
            oConnection.Open();
            string strInvoice = "";
            int intLength = 0;
            string strBookingID = "";

            for (int j = 0; j < objDataset.Tables[0].Rows.Count; j++)
            {
                strInvoice = objDataset.Tables[0].Rows[j][9].ToString();
                intLength = objDataset.Tables[0].Rows[j][9].ToString().Length;
                strBookingID = strInvoice.Substring(6, intLength - 6);

                objCommand = new SqlCommand("UPDATE CORIntInvoice SET OraStatus = 1 WHERE BookingID = '" + strBookingID + "'", oConnection);
                objCommand.CommandTimeout = 600;
                objCommand.ExecuteNonQuery();
            }
            oConnection.Close();
            oConnection.Dispose();
            objCommand.Dispose();
            
            
            DialogResult msgResult = new DialogResult();
            msgResult = MessageBox.Show("Do you want to upload data now.", "Upload in Oracle DB", MessageBoxButtons.OKCancel);

            if (msgResult == DialogResult.OK)
            {
                pnlUpload.Visible = true;
                fn_FillID();
            }
        }
        */
       /* private void fn_FillID()
        {
            // New connection string for local connection
            string strLocalConnection = System.Configuration.ConfigurationManager.AppSettings["localConnectString"];
            OleDbConnection oLocalConnection = new OleDbConnection(strLocalConnection);
            oLocalConnection.Open();
            OleDbCommand lCommand1 = new OleDbCommand("SELECT Top 1 ID FROM  " + strTableName + " WHERE Uploaded = 0 and New_Car <> 'Void Invoices' ORDER BY ID Asc", oLocalConnection);
            int intBeginID = Convert.ToInt32(lCommand1.ExecuteScalar().ToString());
            txtBeginID.Text = intBeginID.ToString();
            hidBeginID.Text = intBeginID.ToString();

            OleDbCommand lCommand2 = new OleDbCommand("SELECT Top 1 ID FROM  " + strTableName + " WHERE Uploaded = 0 and New_Car <> 'Void Invoices' ORDER BY ID Desc", oLocalConnection);
            int intEndID = Convert.ToInt32(lCommand2.ExecuteScalar().ToString());
            txtEndID.Text = intEndID.ToString();
            hidEndID.Text = intEndID.ToString();

            lCommand1.Dispose();
            lCommand2.Dispose();
            oLocalConnection.Close();
            oLocalConnection.Dispose();
        }
        */
        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (txtBeginID.Text == "" || txtEndID.Text == "")
            {
                MessageBox.Show("Enter ID Range.", "ID Range", MessageBoxButtons.OK);
                return;
            }
            if (Convert.ToInt32(txtBeginID.Text) < Convert.ToInt32(hidBeginID.Text))
            {
                MessageBox.Show("ID From is not less than " + hidBeginID.Text);
                return;
            }
            if (Convert.ToInt32(txtEndID.Text) > Convert.ToInt32(hidEndID.Text))
            {
                MessageBox.Show("ID To is not greater than " + hidEndID.Text);
                return;
            }

            int inv_ctr = 0;

            // For uploading data in oracle
            // Create connection with local DB
            string strLocalConnection = System.Configuration.ConfigurationManager.AppSettings["localConnectString"];
            OleDbConnection oLocalConnection = new OleDbConnection(strLocalConnection);

            StringBuilder strID = new StringBuilder("select * from ID_Code");
            oLocalConnection.Open();
            OleDbCommand cmdID = new OleDbCommand(strID.ToString(), oLocalConnection);
            
            OleDbDataReader comprec = cmdID.ExecuteReader();
            comprec.Read();
            int ctr = Convert.ToInt32(comprec["ID"].ToString());
            //ulong
            //int ctr = 1;// Convert.ToInt32(comprec["ID"].ToString());

            StringBuilder strLocalQuery = new StringBuilder(" SELECT * FROM " + strTableName );
            strLocalQuery.Append(" WHERE id >= '"+ txtBeginID.Text +"' ");
            strLocalQuery.Append(" AND id <= '"+ txtEndID.Text +"' AND Uploaded = 0 and New_Car <> 'Void Invoices' ");
            OleDbCommand cmdLocal = new OleDbCommand(strLocalQuery.ToString(), oLocalConnection);
            OleDbDataReader comprec15 = cmdLocal.ExecuteReader();

            // Create connection with oracle DB
            string strConnection = System.Configuration.ConfigurationManager.AppSettings["oracleConnection"];
            OracleConnection oConnection = new OracleConnection(strConnection);
            string strDelete1 = "DELETE FROM RA_INTERFACE_LINES_ALL";
            string strDelete2 = "DELETE FROM RA_INTERFACE_DISTRIBUTIONS_ALL";
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();
            oConnection.Open();
            OracleCommand cmdDelete1 = new OracleCommand(strDelete1, oConnection);
            cmdDelete1.ExecuteNonQuery();
            OracleCommand cmdDelete2 = new OracleCommand(strDelete2, oConnection);
            cmdDelete2.ExecuteNonQuery();
            oConnection.Close();

            if (comprec15 != null)
            {
                // Get data from Invoice_Dec table
                while (comprec15.Read())
                {
                 
                    OleDbCommand getLocation=new OleDbCommand("select id from location where name = '" + Convert.ToString(comprec15[43]) + "'", oLocalConnection);
                    OleDbDataReader locationid = getLocation.ExecuteReader();

                    while (locationid.Read())
                    {
                        IDlocationnew = Convert.ToInt64(locationid[0].ToString());
                        break;
                        //IDlocationnew = Convert.ToInt64((locationid).ToString());
                    }
                        OleDbCommand getProduct = new OleDbCommand("select flex_value from product where product = '" + Convert.ToString(comprec15[31]) + "'", oLocalConnection);
                    OleDbDataReader productid = getProduct.ExecuteReader();
                    
                    while (productid.Read())
                    {
                    IDproductnew = Convert.ToInt64(productid[0].ToString());
                        break;
                    }

                    ctr++;
                    string new_car = "";
                    string strSegment3 = "";
                    if (Convert.ToBoolean(comprec15["Owned"].ToString()) == true)
                    {
                        OleDbCommand cmdCar = new OleDbCommand("select * from new_car where vehicle_no = '" + comprec15["vehicle_no"].ToString() + "'", oLocalConnection);
                        OleDbDataReader newcomprec = cmdCar.ExecuteReader();
                        //newcomprec.Read();
                        //if (newcomprec != null)
                        if (newcomprec.Read())
                        {
                            new_car = newcomprec["car_no"].ToString();
                        }
                        else
                        {
                            new_car = "0";
                        }
                        strSegment3 = "30001";
                    }

                    if (Convert.ToBoolean(comprec15["Owned"].ToString()) == false)
                    {
                        new_car = "Vendor Cateogory";
                        strSegment3 = "30002";
                    }

                    string strPickUp = "";
                    string strClientName = "";

                    string strcmd = "select distinct(id) from auto_Import where RMS_Cust = '" + comprec15["company"].ToString() + "'";
                    cmdID = new OleDbCommand(strcmd, oLocalConnection);
                    OleDbDataReader comprec1 = cmdID.ExecuteReader();
                    comprec1.Read();

                    string strBranch = "select * from inv_type where branch_Name = '" + comprec15["branch_bill_to"].ToString() + "' and CREDIT_TDS_CC = 'STANDARD' ";
                    OleDbCommand cmdBranch = new OleDbCommand(strBranch, oLocalConnection);
                    OleDbDataReader comprec7 = cmdBranch.ExecuteReader();
                    //comprec7.Read();

                    string Cipl_Type = "";
                    //if (comprec7 != null)
                    //Updated by Rahul
                    if (comprec7.Read())
                        Cipl_Type = comprec7["Inv_Type"].ToString();
                    // Insert record in Oracle database
                    StringBuilder strOraInsert;
                    StringBuilder strOraInsert1;
                    StringBuilder strOraInsert2;

                    if (comprec1 != null)
                    {
                        string strOutStation = "";
                        string strDirect = "";
                        if (Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) > 0)
                            strOutStation = "Yes";
                        else
                            strOutStation = "No";

                        if (Convert.ToBoolean(comprec15["direct"].ToString()) == true)
                            strDirect = "Yes";
                        else
                            strDirect = "No";

                        if (comprec15["Pickup"].ToString().Trim().Length > 150)
                            strPickUp = (comprec15["Pickup"].ToString().Trim()).Substring(0, 150);
                        else
                            strPickUp = comprec15["Pickup"].ToString().Trim();

                        


                        if (comprec15["Client_Name"].ToString().Trim().Length > 60)
                            strClientName = (comprec15["Client_Name"].ToString().Trim().ToUpper()).Substring(0, 60);
                        else
                            strClientName = comprec15["Client_Name"].ToString().Trim().ToUpper();

                        if (oConnection.State == ConnectionState.Open)
                            oConnection.Close();
                        
                        oConnection.Open();
                        OracleTransaction oraTransaction = oConnection.BeginTransaction();
                        try
                        {
                            strOraInsert = new StringBuilder(" INSERT INTO RA_INTERFACE_LINES_ALL ");
                            //strOraInsert.Append(" (LINE_TYPE, Description, amount, Term_Id, ORIG_SYSTEM_SHIP_CUSTOMER_REF ");
                            strOraInsert.Append(" (INTERFACE_LINE_ID, LINE_TYPE, Description, amount, Term_Id, ORIG_SYSTEM_SHIP_CUSTOMER_REF ");
                            strOraInsert.Append(" , ORIG_SYSTEM_SHIP_ADDRESS_REF, ORIG_SYSTEM_BILL_CUSTOMER_REF ");
                            strOraInsert.Append(" , ORIG_SYSTEM_BILL_ADDRESS_REF, RECEIPT_METHOD_ID, CONVERSION_TYPE, CONVERSION_RATE ");
                            strOraInsert.Append(" , TRX_DATE, gl_date, QUANTITY, UNIT_SELLING_PRICE, UNIT_STANDARD_PRICE ");
                            strOraInsert.Append(" , ATTRIBUTE1, UOM_CODE, UOM_NAME, ORG_ID, HEADER_ATTRIBUTE_CATEGORY ");
                            strOraInsert.Append(" , HEADER_ATTRIBUTE1, HEADER_ATTRIBUTE2, HEADER_ATTRIBUTE3 ");
                            strOraInsert.Append(" , HEADER_ATTRIBUTE4, HEADER_ATTRIBUTE5, HEADER_ATTRIBUTE6 ");
                            strOraInsert.Append(" , HEADER_ATTRIBUTE7, HEADER_ATTRIBUTE8, HEADER_ATTRIBUTE9 ");
                            strOraInsert.Append(" , HEADER_ATTRIBUTE10, HEADER_ATTRIBUTE11, HEADER_ATTRIBUTE12");
                            strOraInsert.Append(" , HEADER_ATTRIBUTE13, HEADER_ATTRIBUTE14, HEADER_ATTRIBUTE15 ");
                            strOraInsert.Append(" , INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1, INTERFACE_LINE_ATTRIBUTE2 ");
                            strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4, INTERFACE_LINE_ATTRIBUTE5 ");
                            strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7, BATCH_SOURCE_NAME ");
                            strOraInsert.Append(" , trx_number, PURCHASE_ORDER, SET_OF_BOOKS_ID, CURRENCY_CODE ");
                            strOraInsert.Append(" , CUST_TRX_TYPE_NAME) ");
                            //strOraInsert.Append(" VALUES('LINE', 'Standard Package', " + Math.Round(Convert.ToDecimal(comprec15["basic"].ToString()),2));
                            strOraInsert.Append(" VALUES("+ ctr + ", 'LINE', 'Standard Package', " + Math.Round(Convert.ToDecimal(comprec15["basic"].ToString()),2));
                            strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Term_Id"].ToString()) + ", " + Convert.ToInt32(comprec1["ID"].ToString()));
                            strOraInsert.Append(" , '" + comprec15["ship_to"].ToString().ToUpper() + "-" + Convert.ToInt32(comprec1["ID"].ToString()) + "' ");
                            strOraInsert.Append(" , " + Convert.ToInt32(comprec1["ID"].ToString()) + ", '" + comprec15["Branch_Booked"].ToString().ToUpper() + "-" + Convert.ToInt32(comprec1["ID"].ToString()) + "' ");
                            strOraInsert.Append(" , " + Convert.ToInt32(comprec15["recipt_id"].ToString()) + ", 'User', 1, TO_DATE('" + comprec15["VDATE"].ToString() + "', 'mm/dd/yyyy:hh:mi:ssam') ");
                            strOraInsert.Append(" , TO_DATE('" + comprec15["VDATE"].ToString() + "', 'mm/dd/yyyy:hh:mi:ssam') , 1, " + Math.Round(Convert.ToDecimal(comprec15["basic"].ToString()),2));
                            strOraInsert.Append(" , " + Math.Round(Convert.ToDecimal(comprec15["basic"].ToString()),2) + ", " + ctr + ", 'EA', 'Each', 82, 'CIPL Invoice Header' ");
                            strOraInsert.Append(" , 'Corporate', '" + comprec15["Client_Name"].ToString().ToUpper() + "' ");
                            strOraInsert.Append(" , '" + comprec15["Cat"].ToString().ToUpper() + "', '" + comprec15["Category"].ToString().ToUpper() + "' ");
                            strOraInsert.Append(" , '" + strPickUp + "',  TO_DATE('" + comprec15["Car_Used_Date"].ToString() + "', 'mm/dd/yyyy:hh:mi:ssam') ");
                            strOraInsert.Append(" , '" + comprec15["Driver"].ToString().ToUpper().Trim() + "', '" + comprec15["Assignment"].ToString().ToUpper().Trim() + "'");
                            strOraInsert.Append(" , '" + comprec15["Package_Name"].ToString().ToUpper().Trim() + "', " + Convert.ToInt32(comprec15["KM_out"].ToString().Trim()));
                            strOraInsert.Append(" , " + Convert.ToInt32(comprec15["KM_in"].ToString().Trim()) + ", '" + comprec15["Reason_Delay"].ToString().Trim() + "' ");
                            strOraInsert.Append(" , '" + comprec15["Reason_Cancel"].ToString().Trim() + "', '" + strOutStation + "' ");
                            strOraInsert.Append(" , '" + strDirect + "', 'CIPL Transaction Line', '" + comprec15["Duty_Slip_No"].ToString() + "' ");
                            strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                            strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                            strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()));
                            strOraInsert.Append(" , 'CIPL Import1', '" + comprec15["invoice_no"].ToString() + "', '" + strClientName + "' ");
                            strOraInsert.Append(" , 2, 'INR', '" + Cipl_Type + "') ");

                            OracleCommand oraCommand = new OracleCommand(strOraInsert.ToString(), oConnection, oraTransaction);
                            oraCommand.ExecuteNonQuery();
                        }
                        catch(Exception ex)
                        {
                            oraTransaction.Rollback();
                            continue;
                        }

                        StringBuilder strQuery1 = new StringBuilder(" Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB ");
                        strQuery1.Append(" Where  FVB.FLEX_VALUE_SET_ID = 1008074 ");
                        strQuery1.Append(" AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID ");
                        strQuery1.Append(" AND FVTB.Description = '" + comprec15["Ship_To1"].ToString() + "'");
                        OracleCommand cmdQuery1 = new OracleCommand(strQuery1.ToString(), oConnection, oraTransaction);
                        OracleDataReader comprec3 = cmdQuery1.ExecuteReader();
                        comprec3.Read();

                        StringBuilder strQuery2 = new StringBuilder(" Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB ");
                        strQuery2.Append(" Where  FVB.FLEX_VALUE_SET_ID = 1008075 ");
                        strQuery2.Append(" AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID ");
                        strQuery2.Append(" AND FVTB.Description = '" + new_car + "'");
                        OracleCommand cmdQuery2 = new OracleCommand(strQuery2.ToString(), oConnection, oraTransaction);
                        OracleDataReader comprec4 = cmdQuery2.ExecuteReader();
                        comprec4.Read();

                        StringBuilder strQuery3 = new StringBuilder(" Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB ");
                        strQuery3.Append(" Where  FVB.FLEX_VALUE_SET_ID = 1008077 ");
                        strQuery3.Append(" AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID ");
                        strQuery3.Append(" AND FVTB.Description = '" + comprec15["Product"].ToString() + "'");
                        OracleCommand cmdQuery3 = new OracleCommand(strQuery3.ToString(), oConnection, oraTransaction);
                        OracleDataReader comprec5 = cmdQuery3.ExecuteReader();
                        comprec5.Read();

                        // Insert into second table
                        // Distribution Table to Be insert Here
                        try
                        {
                            strOraInsert1 = new StringBuilder(" INSERT INTO RA_INTERFACE_DISTRIBUTIONS_ALL (ACCOUNT_CLASS ");
                            strOraInsert1.Append(" , amount, Percent, segment1, SEGMENT2, SEGMENT4, SEGMENT3, ORG_ID ");
                            strOraInsert1.Append(" , INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1, INTERFACE_LINE_ATTRIBUTE2 ");
                            strOraInsert1.Append(" , INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4, INTERFACE_LINE_ATTRIBUTE5 ");
                            strOraInsert1.Append(" , INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7) ");

                            strOraInsert1.Append(" VALUES ('REV', " + Math.Round(Convert.ToDecimal(comprec15["basic"].ToString()),2) + ", 100, '" + comprec3["FLEX_VALUE"].ToString() + "' ");
                            strOraInsert1.Append(" , '" + comprec4["FLEX_VALUE"].ToString() + "', '" + comprec5["FLEX_VALUE"].ToString() + "' ");
                            strOraInsert1.Append(" , '" + strSegment3 + "', 82, 'CIPL Transaction Line', '" + comprec15["Duty_Slip_No"].ToString() + "' ");
                            strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                            strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                            strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ")");

                            OracleCommand oraCommand1 = new OracleCommand(strOraInsert1.ToString(), oConnection, oraTransaction);
                            oraCommand1.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            oraTransaction.Rollback();
                            continue;
                        }

                        strQuery2 = new StringBuilder(" Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB ");
                        strQuery2.Append(" Where  FVB.FLEX_VALUE_SET_ID = 1008074 ");
                        strQuery2.Append(" AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID ");
                        strQuery2.Append(" AND FVTB.Description = '" + comprec15["branch_bill_to"].ToString() + "'");
                        cmdQuery2 = new OracleCommand(strQuery2.ToString(), oConnection, oraTransaction);
                        OracleDataReader comprec6 = cmdQuery2.ExecuteReader();
                        comprec6.Read();

                        string strSegment1 = "";
                        string strSegment2 = "";
                        string strSegment4 = "";

                        if (comprec6 != null)
                            strSegment1 = comprec6["FLEX_VALUE"].ToString();

                        if (comprec4 != null)
                            strSegment2 = comprec4["FLEX_VALUE"].ToString();

                        if (comprec5 != null)
                            strSegment4 = comprec5["FLEX_VALUE"].ToString();

                        // Distribution Line For Receivale Entry
                        // Insert into table
                        try
                        {
                            strOraInsert2 = new StringBuilder(" INSERT INTO RA_INTERFACE_DISTRIBUTIONS_ALL (ACCOUNT_CLASS ");
                            strOraInsert2.Append(" , amount, Percent, segment1, SEGMENT2, SEGMENT4, SEGMENT3, ORG_ID ");
                            strOraInsert2.Append(" , INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1, INTERFACE_LINE_ATTRIBUTE2 ");
                            strOraInsert2.Append(" , INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4, INTERFACE_LINE_ATTRIBUTE5 ");
                            strOraInsert2.Append(" , INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7) ");

                            strOraInsert2.Append(" VALUES ('REC', " + Math.Round(Convert.ToDecimal(comprec15["basic"].ToString()),2) + ", 100, '" + strSegment1 + "' ");
                            strOraInsert2.Append(" , '" + strSegment2 + "', '" + strSegment4 + "', '13001', 82 ");
                            strOraInsert2.Append(" , 'CIPL Transaction Line', '" + comprec15["Duty_Slip_No"].ToString() + "' ");
                            strOraInsert2.Append(" , " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                            strOraInsert2.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                            strOraInsert2.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ")");

                            OracleCommand oraCommand2 = new OracleCommand(strOraInsert2.ToString(), oConnection, oraTransaction);
                            oraCommand2.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            oraTransaction.Rollback();
                            continue;
                        }

                        ctr = ctr + 1;
                        int p_Ctr = 0;
                        // **************************NOT INCLUDEDE IN OUTSTANDING*********************
                        // --------Line2-------------------------------------------------------
                        if ((Convert.ToInt32(comprec15["Parking_Toll"].ToString()) + Convert.ToInt32(comprec15["other_taxes"].ToString())) > 0)
                        {
                            string strHEADER_ATTRIBUTE14 = "No";
                            string strHEADER_ATTRIBUTE15 = "No";

                            if (Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) > 0)
                                strHEADER_ATTRIBUTE14 = "Yes";

                            if (Convert.ToBoolean(comprec15["direct"].ToString()) == true)
                                strHEADER_ATTRIBUTE15 = "Yes";

                            try
                            {
                                strOraInsert = new StringBuilder(" INSERT INTO RA_INTERFACE_LINES_ALL (INTERFACE_LINE_ID, LINE_TYPE, Description");
                                strOraInsert.Append(" , amount, Term_Id, ORIG_SYSTEM_SHIP_CUSTOMER_REF, ORIG_SYSTEM_SHIP_ADDRESS_REF ");
                                strOraInsert.Append(" , ORIG_SYSTEM_BILL_CUSTOMER_REF, ORIG_SYSTEM_BILL_ADDRESS_REF, RECEIPT_METHOD_ID ");
                                strOraInsert.Append(" , CONVERSION_TYPE, CONVERSION_RATE, TRX_DATE, gl_date, QUANTITY, UNIT_SELLING_PRICE ");
                                strOraInsert.Append(" , UNIT_STANDARD_PRICE, ATTRIBUTE1, UOM_CODE, UOM_NAME, ORG_ID, trx_number ");
                                strOraInsert.Append(" , PURCHASE_ORDER, HEADER_ATTRIBUTE_CATEGORY, HEADER_ATTRIBUTE1 ");
                                strOraInsert.Append(" , HEADER_ATTRIBUTE2, HEADER_ATTRIBUTE3, HEADER_ATTRIBUTE4 ");
                                strOraInsert.Append(" , HEADER_ATTRIBUTE5, HEADER_ATTRIBUTE6, HEADER_ATTRIBUTE7 ");
                                strOraInsert.Append(" , HEADER_ATTRIBUTE8, HEADER_ATTRIBUTE9, HEADER_ATTRIBUTE10 ");
                                strOraInsert.Append(" , HEADER_ATTRIBUTE11, HEADER_ATTRIBUTE12, HEADER_ATTRIBUTE13 ");
                                strOraInsert.Append(" , HEADER_ATTRIBUTE14, HEADER_ATTRIBUTE15, INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1 ");
                                strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE2, INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4 ");
                                strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE5, INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7 ");
                                strOraInsert.Append(" , BATCH_SOURCE_NAME, SET_OF_BOOKS_ID, CURRENCY_CODE, CUST_TRX_TYPE_NAME) ");
                                strOraInsert.Append(" VALUES("+ ctr + ", 'LINE', 'Parking & Toll Charges', " + (Convert.ToInt32(comprec15["Parking_Toll"].ToString()) + Convert.ToInt32(comprec15["other_taxes"].ToString())));
                                //strOraInsert.Append(" VALUES("'LINE', 'Parking & Toll Charges', " + (Convert.ToInt32(comprec15["Parking_Toll"].ToString()) + Convert.ToInt32(comprec15["other_taxes"].ToString())));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Term_Id"].ToString()) + ", " + Convert.ToInt32(comprec1["ID"].ToString()) + ", '" + comprec15["ship_to"].ToString().ToUpper() + "-" + Convert.ToInt32(comprec1["ID"].ToString()) + "' ");
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec1["ID"].ToString()) + ", '" + comprec15["Branch_Booked"].ToString().ToUpper() + "-" + Convert.ToInt32(comprec1["ID"].ToString()) + "' ");
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["recipt_id"].ToString()) + ", 'User', 1, TO_DATE('" + comprec15["VDATE"].ToString() + "', 'mm/dd/yyyy:hh:mi:ssam'), TO_DATE('" + comprec15["VDATE"].ToString() + "', 'mm/dd/yyyy:hh:mi:ssam') ");
                                strOraInsert.Append(" , 1, " + (Convert.ToInt32(comprec15["Parking_Toll"].ToString()) + Convert.ToInt32(comprec15["other_taxes"].ToString())));
                                strOraInsert.Append(" , " + (Convert.ToInt32(comprec15["Parking_Toll"].ToString()) + Convert.ToInt32(comprec15["other_taxes"].ToString())));
                                strOraInsert.Append(" , " + (ctr - 1) + ", 'EA', 'Each', 82, '" + comprec15["invoice_no"].ToString() + "' ");
                                strOraInsert.Append(" , '" + strClientName + "', 'CIPL Invoice Header' ");
                                strOraInsert.Append(" , 'Corporate', '" + strClientName + "', '" + comprec15["Cat"].ToString().ToUpper() + "' ");
                                strOraInsert.Append(" , '" + comprec15["Category"].ToString().ToUpper() + "', '" + strPickUp + "' ");
                                strOraInsert.Append(" , TO_DATE('" + comprec15["Car_Used_Date"].ToString() + "', 'mm/dd/yyyy:hh:mi:ssam'), '" + comprec15["Driver"].ToString().ToUpper() + "' ");
                                strOraInsert.Append(" , '" + comprec15["Assignment"].ToString().ToUpper() + "', '" + comprec15["Package_Name"].ToString().ToUpper() + "' ");
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["KM_out"].ToString()) + ", " + Convert.ToInt32(comprec15["KM_in"].ToString()) + ", '" + comprec15["Reason_Delay"].ToString() + "' ");
                                strOraInsert.Append(" , '" + comprec15["Reason_Cancel"].ToString() + "', '" + strHEADER_ATTRIBUTE14 + "', '" + strHEADER_ATTRIBUTE15 + "' ");
                                strOraInsert.Append(" , 'CIPL Transaction Line', " + ctr + ", " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()) + ", " + Convert.ToInt32(comprec15["Night"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ", 'CIPL Import1', 2, 'INR', '" + Cipl_Type + "') ");

                                p_Ctr = ctr;

                                OracleCommand oraCommand2 = new OracleCommand(strOraInsert.ToString(), oConnection, oraTransaction);
                                oraCommand2.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                oraTransaction.Rollback();
                                continue;
                            }

                            // ----------------For Distribution
                            try
                            {
                                strOraInsert1 = new StringBuilder(" INSERT INTO RA_INTERFACE_DISTRIBUTIONS_ALL (ACCOUNT_CLASS ");
                                strOraInsert1.Append(" , amount, Percent, segment1, SEGMENT2, SEGMENT4, SEGMENT3, ORG_ID ");
                                strOraInsert1.Append(" , INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1, INTERFACE_LINE_ATTRIBUTE2 ");
                                strOraInsert1.Append(" , INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4, INTERFACE_LINE_ATTRIBUTE5 ");
                                strOraInsert1.Append(" , INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7) ");

                                strOraInsert1.Append(" VALUES ('REV', " + (Convert.ToInt32(comprec15["Parking_Toll"].ToString()) + Convert.ToInt32(comprec15["other_taxes"])) + ", 100, '" + comprec3["FLEX_VALUE"] + "' ");
                                strOraInsert1.Append(" , '" + comprec4["FLEX_VALUE"].ToString() + "', '" + comprec5["FLEX_VALUE"] + "' ");
                                strOraInsert1.Append(" , '40301', 82, 'CIPL Transaction Line', " + ctr);
                                strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ")");

                                OracleCommand oraCommand1 = new OracleCommand(strOraInsert1.ToString(), oConnection, oraTransaction);
                                oraCommand1.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                oraTransaction.Rollback();
                                continue;
                            }
                        }

                        // ---------------------------fOR TAX LINE---------------------------------------
                        if (Convert.ToDecimal(comprec15["STAX"].ToString()) != 0)
                        {
                            ctr = ctr + 1;
                            double decAmount = Math.Round((Convert.ToDouble(comprec15["basic"].ToString()) * Convert.ToDouble(comprec15["Service_Tax"].ToString())) / 100, 2);

                            try
                            {
                                strOraInsert = new StringBuilder(" INSERT INTO RA_INTERFACE_LINES_ALL (INTERFACE_LINE_ID, LINE_TYPE, Description");
                                strOraInsert.Append(" , amount, CONVERSION_TYPE, CONVERSION_RATE, TRX_DATE, TAX_RATE ");
                                strOraInsert.Append(" , TAX_CODE, TAX_PRECEDENCE, ATTRIBUTE1, ORG_ID, INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1 ");
                                strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE2, INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4 ");
                                strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE5, INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7 ");
                                strOraInsert.Append(" , BATCH_SOURCE_NAME, SET_OF_BOOKS_ID, CURRENCY_CODE, CUST_TRX_TYPE_NAME ");
                                strOraInsert.Append(" , LINK_TO_LINE_CONTEXT, LINK_TO_LINE_ATTRIBUTE1, LINK_TO_LINE_ATTRIBUTE2, LINK_TO_LINE_ATTRIBUTE3 ");
                                strOraInsert.Append(" , LINK_TO_LINE_ATTRIBUTE4, LINK_TO_LINE_ATTRIBUTE5, LINK_TO_LINE_ATTRIBUTE6, LINK_TO_LINE_ATTRIBUTE7) ");

                                strOraInsert.Append(" VALUES("+ ctr + ", 'TAX', 'Tax line', " + decAmount + ", 'User', 1, TO_DATE('" + comprec15["VDATE"].ToString() + "', 'mm/dd/yyyy:hh:mi:ssam')");
                                strOraInsert.Append(" , '" + (comprec15["Service_Tax"].ToString())+"'");
                                strOraInsert.Append(" , "  + "'Service Tax @ " + (comprec15["Service_Tax"].ToString()) + "%', 0 ");
                                strOraInsert.Append(" , " + ctr + ", 82, 'CIPL Transaction Line', " + ctr + ", " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ", 'CIPL Import1' ");
                                strOraInsert.Append(" , 2, 'INR', '" + Cipl_Type + "', 'CIPL Transaction Line', '" + comprec15["Duty_Slip_No"] + "', " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ")");

                                OracleCommand oraCommand2 = new OracleCommand(strOraInsert.ToString(), oConnection, oraTransaction);
                                oraCommand2.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                oraTransaction.Rollback();
                                continue;
                            }

                            // ----------------For Distribution
                            try
                            {
                                strOraInsert1 = new StringBuilder(" INSERT INTO RA_INTERFACE_DISTRIBUTIONS_ALL (ACCOUNT_CLASS ");
                                strOraInsert1.Append(" , amount, Percent, segment1, SEGMENT2, SEGMENT3, SEGMENT4, ORG_ID ");
                                strOraInsert1.Append(" , INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1, INTERFACE_LINE_ATTRIBUTE2 ");
                                strOraInsert1.Append(" , INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4, INTERFACE_LINE_ATTRIBUTE5 ");
                                strOraInsert1.Append(" , INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7) ");

                                strOraInsert1.Append(" VALUES ('TAX', " + decAmount + ", 100," + IDlocationnew + ", '00000', '25004'," + IDproductnew +" ");
                                //strOraInsert1.Append(" VALUES ('TAX', " + decAmount + ", 100, '000', '00000', '25004', '00' ");
                                strOraInsert1.Append(" , 82, 'CIPL Transaction Line', " + ctr);
                                strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ")");

                                OracleCommand oraCommand1 = new OracleCommand(strOraInsert1.ToString(), oConnection, oraTransaction);
                                oraCommand1.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                oraTransaction.Rollback();
                                continue;
                            }

                            // --------------------------------fOR cESS cALCULATION---------------------------
                            ctr = ctr + 1;

                            decAmount = Math.Round((Convert.ToDouble(comprec15["basic"].ToString()) * Convert.ToDouble(comprec15["cess_Tax"].ToString())) / 100, 2);

                            try
                            {
                                strOraInsert = new StringBuilder(" INSERT INTO RA_INTERFACE_LINES_ALL (INTERFACE_LINE_ID, LINE_TYPE, Description");
                                strOraInsert.Append(" , amount, CONVERSION_TYPE, CONVERSION_RATE, TRX_DATE, TAX_RATE ");
                                strOraInsert.Append(" , TAX_CODE, TAX_PRECEDENCE, ATTRIBUTE1, ORG_ID, INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1 ");
                                strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE2, INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4 ");
                                strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE5, INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7 ");
                                strOraInsert.Append(" , BATCH_SOURCE_NAME, SET_OF_BOOKS_ID, CURRENCY_CODE, CUST_TRX_TYPE_NAME ");
                                strOraInsert.Append(" , LINK_TO_LINE_CONTEXT, LINK_TO_LINE_ATTRIBUTE1, LINK_TO_LINE_ATTRIBUTE2, LINK_TO_LINE_ATTRIBUTE3 ");
                                strOraInsert.Append(" , LINK_TO_LINE_ATTRIBUTE4, LINK_TO_LINE_ATTRIBUTE5, LINK_TO_LINE_ATTRIBUTE6, LINK_TO_LINE_ATTRIBUTE7) ");

                                strOraInsert.Append(" VALUES(" +ctr + ", 'TAX', 'Tax line', " + decAmount + ", 'User', 1, TO_DATE('" + comprec15["VDATE"].ToString() + "', 'mm/dd/yyyy:hh:mi:ssam') ");
                                //strOraInsert.Append(" , 2, 'Service Tax Education Cess @ 3%', 1 ");
                                //updated by Rahul
                                strOraInsert.Append(" , 3 , 'Service Tax Education Cess @ 3%' , 1 ");
                                strOraInsert.Append(" , " + ctr + ", 82, 'CIPL Transaction Line', " + ctr + ", " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ", 'CIPL Import1' ");
                                strOraInsert.Append(" , 2, 'INR', '" + Cipl_Type + "', 'CIPL Transaction Line', '" + comprec15["Duty_Slip_No"].ToString() + "', " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ")");

                                OracleCommand oraCommand2 = new OracleCommand(strOraInsert.ToString(), oConnection, oraTransaction);
                                oraCommand2.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                oraTransaction.Rollback();
                                continue;
                            }

                            // ----------------For Distribution
                            try
                            {
                                strOraInsert1 = new StringBuilder(" INSERT INTO RA_INTERFACE_DISTRIBUTIONS_ALL (ACCOUNT_CLASS ");
                                strOraInsert1.Append(" , amount, Percent, segment1, SEGMENT2, SEGMENT3, SEGMENT4, ORG_ID ");
                                strOraInsert1.Append(" , INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1, INTERFACE_LINE_ATTRIBUTE2 ");
                                strOraInsert1.Append(" , INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4, INTERFACE_LINE_ATTRIBUTE5 ");
                                strOraInsert1.Append(" , INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7) ");

                                
                                //strOraInsert1.Append(" VALUES ('TAX', " + decAmount + ", 100, '000', '00000', '25004', '00' ");
                                strOraInsert1.Append(" VALUES ('TAX', " + decAmount + ", 100, " + IDlocationnew + ", '00000', '25004', " + IDproductnew + " ");
                                strOraInsert1.Append(" , 82, 'CIPL Transaction Line', " + ctr);
                                strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ")");

                                OracleCommand oraCommand1 = new OracleCommand(strOraInsert1.ToString(), oConnection, oraTransaction);
                                oraCommand1.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                oraTransaction.Rollback();
                                continue;
                            }
                        }

                        // --------------------------------FOR VAT CALCULATION---------------------------
                        if ((comprec15["CITY_NAME"].ToString().ToUpper() == "DELHI"  && Convert.ToDecimal(comprec15["VAT"].ToString()) > 0) || (comprec15["Company"].ToString() == "Self Drive"))
                        {
                            ctr = ctr + 1;
                            double decAmount = Math.Round((Convert.ToDouble(comprec15["basic"].ToString()) * 12.5) / 100, 2);

                            try
                            {
                                strOraInsert = new StringBuilder(" INSERT INTO RA_INTERFACE_LINES_ALL (INTERFACE_LINE_ID, LINE_TYPE, Description");
                                strOraInsert.Append(" , amount, CONVERSION_TYPE, CONVERSION_RATE, TRX_DATE, TAX_RATE ");
                                strOraInsert.Append(" , TAX_CODE, TAX_PRECEDENCE, ATTRIBUTE1, ORG_ID, INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1 ");
                                strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE2, INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4 ");
                                strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE5, INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7 ");
                                strOraInsert.Append(" , BATCH_SOURCE_NAME, SET_OF_BOOKS_ID, CURRENCY_CODE, CUST_TRX_TYPE_NAME ");
                                strOraInsert.Append(" , LINK_TO_LINE_CONTEXT, LINK_TO_LINE_ATTRIBUTE1, LINK_TO_LINE_ATTRIBUTE2, LINK_TO_LINE_ATTRIBUTE3 ");
                                strOraInsert.Append(" , LINK_TO_LINE_ATTRIBUTE4, LINK_TO_LINE_ATTRIBUTE5, LINK_TO_LINE_ATTRIBUTE6, LINK_TO_LINE_ATTRIBUTE7) ");

                                strOraInsert.Append(" VALUES("+ ctr + ",'TAX', 'Tax line', " + decAmount + ", 'User', 1, TO_DATE('" + comprec15["VDATE"].ToString() + "', 'mm/dd/yyyy:hh:mi:ssam') ");
                                strOraInsert.Append(" , 12.5 , 'VAT @ 12.5%', 0 ");
                                strOraInsert.Append(" , " + ctr + ", 82, 'CIPL Transaction Line', " + ctr + ", " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ", 'CIPL Import1' ");
                                //To be checked
                                strOraInsert.Append(" , 2, 'INR', '" + Cipl_Type + "', 'CIPL Transaction Line', '" + comprec15["Duty_Slip_No"].ToString() + "', " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ")");

                                OracleCommand oraCommand2 = new OracleCommand(strOraInsert.ToString(), oConnection, oraTransaction);
                                oraCommand2.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                oraTransaction.Rollback();
                                continue;
                            }

                            // ----------------For Distribution
                            try
                            {
                                strOraInsert1 = new StringBuilder(" INSERT INTO RA_INTERFACE_DISTRIBUTIONS_ALL (ACCOUNT_CLASS ");
                                strOraInsert1.Append(" , amount, Percent, segment1, SEGMENT2, SEGMENT3, SEGMENT4, ORG_ID ");
                                strOraInsert1.Append(" , INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1, INTERFACE_LINE_ATTRIBUTE2 ");
                                strOraInsert1.Append(" , INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4, INTERFACE_LINE_ATTRIBUTE5 ");
                                strOraInsert1.Append(" , INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7) ");

                                //strOraInsert1.Append(" VALUES ('TAX', " + decAmount + ", 100, '000', '00000', '25007', '00' ");
                                strOraInsert1.Append(" VALUES ('TAX', " + decAmount + ", 100, " + IDlocationnew + ", '00000', '25007', " + IDproductnew + " ");
                                
                                strOraInsert1.Append(" , 82, 'CIPL Transaction Line', " + ctr);
                                strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ")");

                                OracleCommand oraCommand1 = new OracleCommand(strOraInsert1.ToString(), oConnection, oraTransaction);
                                oraCommand1.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                oraTransaction.Rollback();
                                continue;
                            }
                        }

                        if ((Convert.ToDecimal(comprec15["Parking_Toll"].ToString()) + Convert.ToDecimal(comprec15["other_taxes"].ToString())) > 0 && Convert.ToDecimal(comprec15["STAX"].ToString()) != 0)
                        {
                            //  ---------------------------Tax on Parking
                            ctr = ctr + 1;
                            double decAmount = Math.Round(((Convert.ToDouble(comprec15["Parking_Toll"].ToString()) + Convert.ToDouble(comprec15["other_taxes"].ToString())) * Convert.ToDouble(comprec15["Service_Tax"].ToString())) / 100, 2);

                            try
                            {
                                strOraInsert = new StringBuilder(" INSERT INTO RA_INTERFACE_LINES_ALL (INTERFACE_LINE_ID,LINE_TYPE, Description");
                                strOraInsert.Append(" , amount, CONVERSION_TYPE, CONVERSION_RATE, TRX_DATE, TAX_RATE ");
                                strOraInsert.Append(" , TAX_CODE, TAX_PRECEDENCE, ATTRIBUTE1, ORG_ID, INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1 ");
                                strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE2, INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4 ");
                                strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE5, INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7 ");
                                strOraInsert.Append(" , BATCH_SOURCE_NAME, SET_OF_BOOKS_ID, CURRENCY_CODE, CUST_TRX_TYPE_NAME ");
                                strOraInsert.Append(" , LINK_TO_LINE_CONTEXT, LINK_TO_LINE_ATTRIBUTE1, LINK_TO_LINE_ATTRIBUTE2, LINK_TO_LINE_ATTRIBUTE3 ");
                                strOraInsert.Append(" , LINK_TO_LINE_ATTRIBUTE4, LINK_TO_LINE_ATTRIBUTE5, LINK_TO_LINE_ATTRIBUTE6, LINK_TO_LINE_ATTRIBUTE7) ");

                                strOraInsert.Append(" VALUES("+ ctr+ ",'TAX', 'Tax line', " + decAmount + ", 'User', 1, TO_DATE('" + comprec15["VDATE"].ToString() + "', 'mm/dd/yyyy:hh:mi:ssam') ");
                                strOraInsert.Append(" , " + (comprec15["Service_Tax"].ToString()) + " , 'Service Tax @ " + (comprec15["Service_Tax"].ToString()) + "%', 0 ");
                                strOraInsert.Append(" , " + ctr + ", 82, 'CIPL Transaction Line', " + ctr + ", " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ", 'CIPL Import1' ");
                                strOraInsert.Append(" , 2, 'INR', '" + Cipl_Type + "', 'CIPL Transaction Line', " + p_Ctr + ", " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ")");

                                OracleCommand oraCommand2 = new OracleCommand(strOraInsert.ToString(), oConnection, oraTransaction);
                                oraCommand2.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                oraTransaction.Rollback();
                                continue;
                            }

                            // ----------------For Distribution
                            try
                            {
                                strOraInsert1 = new StringBuilder(" INSERT INTO RA_INTERFACE_DISTRIBUTIONS_ALL (ACCOUNT_CLASS ");
                                strOraInsert1.Append(" , amount, Percent, segment1, SEGMENT2, SEGMENT3, SEGMENT4, ORG_ID ");
                                strOraInsert1.Append(" , INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1, INTERFACE_LINE_ATTRIBUTE2 ");
                                strOraInsert1.Append(" , INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4, INTERFACE_LINE_ATTRIBUTE5 ");
                                strOraInsert1.Append(" , INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7) ");

                                //strOraInsert1.Append(" VALUES ('TAX', " + decAmount + ", 100, '000', '00000', '25004', '00' ");
                                strOraInsert1.Append(" VALUES ('TAX', " + decAmount + ", 100, " + IDlocationnew + ", '00000', '25004', " + IDproductnew + " ");

                                
                                strOraInsert1.Append(" , 82, 'CIPL Transaction Line', " + ctr);
                                strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ")");

                                OracleCommand oraCommand1 = new OracleCommand(strOraInsert1.ToString(), oConnection, oraTransaction);
                                oraCommand1.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                oraTransaction.Rollback();
                                continue;
                            }

                            // --------------------------------FOR CESS CALCULATION---------------------------
                            ctr = ctr + 1;
                            decAmount = Math.Round(((Convert.ToDouble(comprec15["Parking_Toll"].ToString()) + Convert.ToDouble(comprec15["other_taxes"].ToString())) * Convert.ToDouble(comprec15["cess_Tax"].ToString())) / 100, 2);
                            
                            //(((Convert.ToInt32(comprec15["Parking_Toll"]) + Convert.ToInt32(comprec15["other_taxes"])) * comprec15["cess_Tax"]) / 100, 2);

                            try
                            {
                                strOraInsert = new StringBuilder(" INSERT INTO RA_INTERFACE_LINES_ALL (INTERFACE_LINE_ID,LINE_TYPE, Description");
                                strOraInsert.Append(" , amount, CONVERSION_TYPE, CONVERSION_RATE, TRX_DATE, TAX_RATE ");
                                strOraInsert.Append(" , TAX_CODE, TAX_PRECEDENCE, ATTRIBUTE1, ORG_ID, INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1 ");
                                strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE2, INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4 ");
                                strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE5, INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7 ");
                                strOraInsert.Append(" , BATCH_SOURCE_NAME, SET_OF_BOOKS_ID, CURRENCY_CODE, CUST_TRX_TYPE_NAME ");
                                strOraInsert.Append(" , LINK_TO_LINE_CONTEXT, LINK_TO_LINE_ATTRIBUTE1, LINK_TO_LINE_ATTRIBUTE2, LINK_TO_LINE_ATTRIBUTE3 ");
                                strOraInsert.Append(" , LINK_TO_LINE_ATTRIBUTE4, LINK_TO_LINE_ATTRIBUTE5, LINK_TO_LINE_ATTRIBUTE6, LINK_TO_LINE_ATTRIBUTE7) ");
                                strOraInsert.Append(" VALUES("+ctr+ ",'TAX', 'Tax line', " + decAmount + ", 'User', 1, TO_DATE('" + comprec15["VDATE"].ToString() + "', 'mm/dd/yyyy:hh:mi:ssam') ");
                                strOraInsert.Append(" , 3 , 'Service Tax Education Cess @ 3%', 1 ");
                                strOraInsert.Append(" , " + ctr + ", 82, 'CIPL Transaction Line', " + ctr + ", " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ", 'CIPL Import1' ");
                                strOraInsert.Append(" , 2, 'INR', '" + Cipl_Type + "', 'CIPL Transaction Line', " + p_Ctr + ", " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ")");

                                OracleCommand oraCommand2 = new OracleCommand(strOraInsert.ToString(), oConnection, oraTransaction);
                                oraCommand2.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                oraTransaction.Rollback();
                                continue;
                            }

                            decAmount = Math.Round(Convert.ToDouble(comprec15["STAX"].ToString()), 2);
                            // ----------------For Distribution
                            try
                            {
                                strOraInsert1 = new StringBuilder(" INSERT INTO RA_INTERFACE_DISTRIBUTIONS_ALL (ACCOUNT_CLASS ");
                                strOraInsert1.Append(" , amount, Percent, segment1, SEGMENT2, SEGMENT3, SEGMENT4, ORG_ID ");
                                strOraInsert1.Append(" , INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1, INTERFACE_LINE_ATTRIBUTE2 ");
                                strOraInsert1.Append(" , INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4, INTERFACE_LINE_ATTRIBUTE5 ");
                                strOraInsert1.Append(" , INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7) ");

                                //strOraInsert1.Append(" VALUES ('TAX', " + decAmount + ", 100, '000', '00000', '25004', '00' ");
                                strOraInsert1.Append(" VALUES ('TAX', " + decAmount + ", 100, " + IDlocationnew + ", '00000', '25004', " + IDproductnew + " ");
                                
                                strOraInsert1.Append(" , 82, 'CIPL Transaction Line', " + ctr);
                                strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ")");

                                OracleCommand oraCommand1 = new OracleCommand(strOraInsert1.ToString(), oConnection, oraTransaction);
                                oraCommand1.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                oraTransaction.Rollback();
                                continue;
                            }

                            // --------------------------------FOR VAT CALCULATION---------------------------
                            //if (comprec15["CITY_NAME"].ToString().ToUpper() == "DELHI" && Convert.ToDecimal(comprec15["VAT"].ToString()) > 0)
                            if ((comprec15["CITY_NAME"].ToString().ToUpper() == "DELHI" && Convert.ToDecimal(comprec15["VAT"].ToString()) > 0) || (comprec15["Company"].ToString() == "Self Drive"))
                            {
                                ctr = ctr + 1;
                                decAmount = Math.Round(Convert.ToDouble(comprec15["VAT"].ToString()), 2);

                                try
                                {
                                    strOraInsert = new StringBuilder(" INSERT INTO RA_INTERFACE_LINES_ALL (INTERFACE_LINE_ID,LINE_TYPE, Description");
                                    strOraInsert.Append(" , amount, CONVERSION_TYPE, CONVERSION_RATE, TRX_DATE, TAX_RATE ");
                                    strOraInsert.Append(" , TAX_CODE, TAX_PRECEDENCE, ATTRIBUTE1, ORG_ID, INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1 ");
                                    strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE2, INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4 ");
                                    strOraInsert.Append(" , INTERFACE_LINE_ATTRIBUTE5, INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7 ");
                                    strOraInsert.Append(" , BATCH_SOURCE_NAME, SET_OF_BOOKS_ID, CURRENCY_CODE, CUST_TRX_TYPE_NAME ");
                                    strOraInsert.Append(" , LINK_TO_LINE_CONTEXT, LINK_TO_LINE_ATTRIBUTE1, LINK_TO_LINE_ATTRIBUTE2, LINK_TO_LINE_ATTRIBUTE3 ");
                                    strOraInsert.Append(" , LINK_TO_LINE_ATTRIBUTE4, LINK_TO_LINE_ATTRIBUTE5, LINK_TO_LINE_ATTRIBUTE6, LINK_TO_LINE_ATTRIBUTE7) ");

                                    strOraInsert.Append(" VALUES("+ctr+ ",'TAX', 'Tax line', " + decAmount + ", 'User', 1, TO_DATE('" + comprec15["VDATE"].ToString() + "', 'mm/dd/yyyy:hh:mi:ssam') ");
                                    strOraInsert.Append(" , 12.5 , 'VAT @ 12.5%', 0 ");
                                    strOraInsert.Append(" , " + ctr + ", 82, 'CIPL Transaction Line', " + ctr + ", " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                    strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                    strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ", 'CIPL Import1' ");
                                    strOraInsert.Append(" , 2, 'INR', '" + Cipl_Type + "', 'CIPL Transaction Line', " + p_Ctr + ", " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                    strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                    strOraInsert.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ")");

                                    OracleCommand oraCommand2 = new OracleCommand(strOraInsert.ToString(), oConnection, oraTransaction);
                                    oraCommand2.ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    oraTransaction.Rollback();
                                    continue;
                                }

                                // ----------------For Distribution
                                try
                                {
                                    strOraInsert1 = new StringBuilder(" INSERT INTO RA_INTERFACE_DISTRIBUTIONS_ALL (ACCOUNT_CLASS ");
                                    strOraInsert1.Append(" , amount, Percent, CODE_COMBINATION_ID, segment1, SEGMENT2, SEGMENT3, SEGMENT4, ORG_ID ");
                                    strOraInsert1.Append(" , INTERFACE_LINE_CONTEXT, INTERFACE_LINE_ATTRIBUTE1, INTERFACE_LINE_ATTRIBUTE2 ");
                                    strOraInsert1.Append(" , INTERFACE_LINE_ATTRIBUTE3, INTERFACE_LINE_ATTRIBUTE4, INTERFACE_LINE_ATTRIBUTE5 ");
                                    strOraInsert1.Append(" , INTERFACE_LINE_ATTRIBUTE6, INTERFACE_LINE_ATTRIBUTE7) ");

                                    //strOraInsert1.Append(" VALUES ('TAX', " + decAmount + ", 100, 1157, '000', '00000', '25007', '00' ");
                                    strOraInsert1.Append(" VALUES ('TAX', " + decAmount + ", 100, 1157, " + IDlocationnew + ", '00000', '25007', " + IDproductnew + " ");
                                    
                                    strOraInsert1.Append(" , 82, 'CIPL Transaction Line', " + ctr);
                                    strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Extra_Km"].ToString()) + ", " + Convert.ToDecimal(comprec15["Extra_Hrs"].ToString()));
                                    strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Outstation_Amt"].ToString()) + ", " + Convert.ToInt32(comprec15["Fuel"].ToString()));
                                    strOraInsert1.Append(" , " + Convert.ToInt32(comprec15["Night"].ToString()) + ", " + Convert.ToInt32(comprec15["other_Charges"].ToString()) + ")");

                                    OracleCommand oraCommand1 = new OracleCommand(strOraInsert1.ToString(), oConnection, oraTransaction);
                                    oraCommand1.ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    oraTransaction.Rollback();
                                    continue;
                                }
                            }
                        }
                        oraTransaction.Commit();
                        oraTransaction.Dispose();
                        //StringBuilder strUpdateQuery = new StringBuilder(" UPDATE Invoice_Dec SET Uploaded = 1 WHERE ID = " + comprec15["ID"].ToString());
                        //OleDbCommand cmdUpdateInv = new OleDbCommand(strUpdateQuery.ToString(), oLocalConnection);
                        //cmdUpdateInv.ExecuteNonQuery();
                    }
                    else
                    {
                        StringBuilder strError = new StringBuilder(" INSERT INTO Error_List (company) VALUES('"+ comprec15["company"].ToString() +"') ");
                        OleDbCommand cmdError = new OleDbCommand(strError.ToString(), oLocalConnection);
                        cmdError.ExecuteNonQuery();
                    }
                    inv_ctr = inv_ctr + 1;
                }// End of comprec15 Loop
            }// End of comprec15
            StringBuilder strUpdate = new StringBuilder(" UPDATE id_code set id = "+ ctr);
            OleDbCommand cmdUpdate = new OleDbCommand(strUpdate.ToString(), oLocalConnection);
            cmdUpdate.ExecuteNonQuery();

            MessageBox.Show("Total " + inv_ctr + " records are uploaded");
            oConnection.Close();
            oLocalConnection.Close();
            cmdDelete1.Dispose();
            cmdDelete2.Dispose();
            cmdID.Dispose();
            cmdLocal.Dispose();
            cmdUpdate.Dispose();
            comprec.Dispose();
            comprec15.Dispose();
        }

        private object Trim(object p)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        private void btnUpdateInvoice_Click(object sender, EventArgs e)
        {
            // For updating uploaded column value
            // Create connection with oracle DB
            string strConnection = System.Configuration.ConfigurationManager.AppSettings["oracleConnection"];
            OracleConnection oConnection = new OracleConnection(strConnection);
            string strOraQuery = "SELECT distinct TRX_Number FROM RA_INTERFACE_LINES_ALL WHERE Interface_Status = 'P'";
            oConnection.Open();
            OracleCommand cmdOraQuery = new OracleCommand(strOraQuery, oConnection);
            OracleDataReader comprec = cmdOraQuery.ExecuteReader();
            int intVal = 0;
            // Create connection with local DB
            string strLocalConnection = System.Configuration.ConfigurationManager.AppSettings["localConnectString"];
            OleDbConnection oLocalConnection = new OleDbConnection(strLocalConnection);

            if (comprec != null)
            {
                string strUpdateQuery = "";
                while (comprec.Read())
                {
                    strUpdateQuery = "UPDATE "+ strTableName +" SET Uploaded = 1 WHERE Invoice_No = '"+ comprec["TRX_Number"] +"' ";
                    if (oLocalConnection.State == ConnectionState.Open)
                        oLocalConnection.Close();
                    oLocalConnection.Open();
                    OleDbCommand cmdUpdate = new OleDbCommand(strUpdateQuery, oLocalConnection);
                    cmdUpdate.ExecuteNonQuery();
                    intVal++;
                }
            }
            oLocalConnection.Close();
            comprec.Dispose();
            cmdOraQuery.Dispose();
            oConnection.Close();
            MessageBox.Show("Total "+ (intVal - 1) +" Invoices updated");
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void pnlUpload_Paint(object sender, PaintEventArgs e)
        {

        }

        
    }
}