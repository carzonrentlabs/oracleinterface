using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OracleClient;

namespace OracleInterface
{
    public partial class testOracle : Form
    {
        public testOracle()
        {
            InitializeComponent();
        }

        private void testOracle_Load(object sender, EventArgs e)
        {
            string strConnection = System.Configuration.ConfigurationManager.AppSettings["oracleConnection"];
            OracleConnection oConnection = new OracleConnection(strConnection);

            OracleCommand cmd = new OracleCommand("select * from ra_interface_lines_all", oConnection);
            OracleDataAdapter adp = new OracleDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
        }
    }
}