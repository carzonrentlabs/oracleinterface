using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace OracleInterface
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string strConnection = System.Configuration.ConfigurationManager.AppSettings["corConnectString"];
            StringBuilder strQuery;
            SqlConnection oConnection = new SqlConnection(strConnection);

            strQuery = new StringBuilder(" SELECT 1 AS RID, t.name as Branch_Bill_To, f.Company as Company, ");
            strQuery.Append(" f.ClientName as Client_Name, ");
            strQuery.Append(" (f.OutStnAmt + f.FuelSurcharge + f.NightStayAmt + f.OtherCharges + f.BasicPkgRate + f.ExtraHrsRate ");
            strQuery.Append("  + f.ExtraKMRate + f.AdjustedAmount) as Basic, f.ParkTollCharges as Parking_Toll, F.InterstateTax as Other_Taxes ");
            strQuery.Append("  , VehicleNo as Vehicle_No, f.DATE_IN as Vdate, InvoiceNo as Actual_Invoice, DATE_OUT as Car_Used_Date ");
            strQuery.Append("  , InvoiceNo as Invoice_No, DutySlipNo as Duty_Slip_No, 0 as STax, '' as Car_Booked_by ");
            strQuery.Append("  , t.Branch as Branch_Booked ");
            strQuery.Append("  , (CASE f.ClientType WHEN 'Cr' Then 0 Else 1 End) as Direct, CarBooked as Cat ");
            strQuery.Append("  , (CASE f.VendorName WHEN 'Own Car' Then 1 Else 0 End) as Owned, CategoryBooked as Category ");
            strQuery.Append("  , KMOut as KM_OUT, KMIn as KM_in, VendorName as Reason_Delay, '' as Reason_Cancel, OutstationYesNo as Outstation ");
            strQuery.Append("  , ExtraKM as Extra_Km, ExtraHr as Extra_Hrs, OutStnAmt as Outstation_Amt, FuelSurcharge as Fuel ");
            strQuery.Append("  , NightStayAmt as Night, OtherCharges as Other_Charges, t.Product as Product ");
            strQuery.Append("  , 0 as Vat, PkgName as Package_Name, '' as Driver, '' as Assignment, PickUpAdd as Pickup ");
            strQuery.Append("  , PickUpCity as CITY_NAME, '' as Remark, PickUpCity as SHIP_TO, '' as INVOICE_TYPE, '' as New_Car ");
            strQuery.Append("  , 0 as Uploaded, (Select Ship_To_1 FROM tblInterface WHERE Ship_To = f.PickUpCity) as Ship_to1, 5 as Term_Id, 1010 as Recipt_ID, ServiceTaxPercent as Service_Tax ");
            strQuery.Append("  , EduCessPercent as Cess_Tax, DSTPercent as Vat_Tax, 1 as OraStatus ");
            strQuery.Append("  FROM   fn_DataForOracleSale('06/01/2007', '06/04/2007', 2) as f ");
            strQuery.Append("  , tblInterface as t ");
            strQuery.Append("  WHERE f.CompanyCityID = t.CityID ");

            SqlCommand objCommand = new SqlCommand(strQuery.ToString(), oConnection);
            SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
            DataSet objDataset = new DataSet();
            objAdapter.Fill(objDataset);
            dtGrid.DataSource = objDataset.Tables[0];
        }

        private void dtGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}